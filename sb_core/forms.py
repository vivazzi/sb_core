from django import forms
from django.core import management

from sb_core.mixins import TemplateFormMixin
from sb_core.widgets import Select2Widget
from sb_core import settings as app_settings


class SEOForm(forms.ModelForm):
    class Meta:
        exclude = ()
        widgets = {
            'meta_tags': forms.Textarea(attrs={'cols': 40, 'rows': 3}),
            'meta_desc': forms.Textarea(attrs={'cols': 40, 'rows': 3}),
        }


class RunManagementCommandForm(forms.Form):
    commands = [('{}.{}'.format(app, command), command) for command, app in management.get_commands().items()]
    command = forms.ChoiceField(label='Список команд', choices=commands, widget=Select2Widget)
    params = forms.CharField(label='Параметры', required=False)

    class Media:
        js = (app_settings.JQUERY_URL, 'sb_core/js/management_commands.js')


class TemplateForm(TemplateFormMixin, forms.ModelForm):
    class Meta:
        exclude = ()
