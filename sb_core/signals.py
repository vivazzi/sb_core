import django.dispatch
from django.core.management import call_command

thumbnail_created = django.dispatch.Signal()


def generate_sitemap(sender, instance, **kwargs):
    call_command('generate_sitemap')
