from django.template.loader import select_template
from django.urls import reverse

from sb_core.template_pool import template_pool


class AdminUrlsMixin(object):
    def get_admin_absolute_url(self):
        return reverse('admin:{}_{}_change'.format(self._meta.app_label, self._meta.model_name), args=(self.id,))

    @classmethod
    def get_admin_changelist_url(cls):
        return reverse('admin:{}_{}_changelist'.format(cls._meta.app_label, cls._meta.model_name))


class ExtensionFixMixin(object):
    def copy_to_public(self, public_object, language):
        if not self.public_extension:
            try:
                self.__class__.objects.get(extended_object_id=public_object.pk).delete()
            except self.__class__.DoesNotExist:
                pass

        return super(ExtensionFixMixin, self).copy_to_public(public_object, language)


class RenderTemplateMixin(object):
    def get_render_template(self, context, instance, placeholder):
        if instance.template:
            template = template_pool.get_template(instance, instance.template)
            if template:
                return select_template([template().get_template(instance, context), 'sb_core/template_is_not_found.html'])

        return self.render_template

    def render(self, context, instance, placeholder):
        context = super(RenderTemplateMixin, self).render(context, instance, placeholder)

        if instance.template:
            template = template_pool.get_template(instance, instance.template)
            if template:
                context.update(template().get_context(instance, context))

        return context


class TemplateFormMixin(object):
    def clean(self):
        template = template_pool.get_template(self.instance, self.cleaned_data['template'])
        if template:
            template().clean(self)
