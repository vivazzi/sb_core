class ImageException(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)


class ThumbnailException(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)


class SendLetterException(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)