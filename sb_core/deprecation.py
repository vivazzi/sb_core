
# USAGE: warnings.warn('Removed in next version', RemovedInNextVersionWarning, stacklevel=2)


class RemovedInNextVersionWarning(DeprecationWarning):
    pass
