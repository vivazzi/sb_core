from os.path import join

from django.conf import settings
from django.utils.translation import gettext as _

# --- cms plugin module names ---
BASE = 'Базовые'
DECOR = 'Оформление'
NAVIGATION = 'Навигация'
ADDITION = 'Дополнительные'
SEARCH = 'Поиск'
FORMS = 'Формы'
PRO = 'PRO'
SPECIFIC = 'Специфические'
TRANSPORT = 'Транспорт'


# --- help texts ---
title_ht = 'Отображается только в административной части'
pic_ht = 'Рекомендуется загружать фото не более 200 KB'

color_ht = ('Примеры: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>'
            'или rgba(0,0,0,0) для прозрачного фона<br/>'
            '<a rel="nofollow" target="_blank" href="http://getcolor.ru/">Сервис по определению цвета</a>')

attr_class_ht = 'Вспомогательный класс для элемента'
slug_ht = 'Часть заголовка, используемая в URL.<br/>При возникновении ошибки о существовании пути, придумайте новый. Например: path-2'

shadow_ht = ('<сдвиг по x> <сдвиг по y> <радиус размытия> <растяжение> <цвет></br>'
             'Например: 0 0 10px 0 rgba(0, 0, 0, 0.5), 2px 2px 7px 1px rgba(0, 0, 0, 0.4)')

hint_ht = 'Появляется при наведении мышкой на объект'

rel_ht = ('По умолчанию переход поисковыми роботами по внешним ссылкам запрещён, а для внутренних - разрешён.<br/>'
          'Это позволяет не снижать позиции сайта в поисковых системах.<br/>'
          'Подробнее на странице: '
          '<a rel="nofollow" target="_blank" href="https://vits.pro/info/site-positions/">Позиции сайта в поисковых системах</a>')

order_ht = 'Если значение не указано, то порядок будет определён автоматически'
# --- for bleach ---
ALLOWED_TAGS = ('p', 'em', 'strong', 'blockquote', 'a', 'br', 'img', 'pre', 'code', 'table', 'tr', 'th', 'td', 'ul', 'ol', 'li')

# --- alphabet and digits ---
ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
DIGITS = '0123456789'
ALPHABET_AND_DIGITS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'


# --- for full screen image in modal window ---
MAX_THUMB_WIDTH = getattr(settings, 'MAX_THUMB_WIDTH', 1600)
MAX_THUMB_HEIGHT = getattr(settings, 'MAX_THUMB_HEIGHT', 1000)

# --- forms ---
DEFAULT_MANAGER_GROUP = 'Менеджеры'
MODERATORS = 'Модераторы'

# --- others ---
NULL_BOOLEAN_DEFAULT_CHOICES = ((None, _('Default')), (True, _('Yes')), (False, _('No')))

NOW_STR_FORMAT = '%d_%m_%Y__%H_%M_%S'
NEED_BACKUP = 'need_backup'

PAGE_MENU_LAST_BREAK_BEFORE_DELETE_PAGE = 'Page Menu Last Break Before Delete Page'

# --- Cache keys ---
VENV_PATH_KEY = 'sb_core__venv_path'

ROBOTS_PATH = join(settings.MEDIA_ROOT, 'robots.txt')
SITEMAP_PATH = join(settings.MEDIA_ROOT, 'sitemap.xml')

STAT_LOG = 'stat.log'
LOCKFILE = 'lockfile'
