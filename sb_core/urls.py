from os import environ

from django.urls import path, include

from sb_core.views import (info_dashboard, download_db_file, get_given_size, get_backups, do_backup,
                           seo, get_sitemap, get_robots, do_ping_google, generate_robots, generate_sitemap,
                           generate_sitemap_and_ping_google, UtilsView, RunManagementCommandView, UsedComponentsView)

urlpatterns = (
    path('admin/sbl/', include([
        # --- info_dashboard ---
        path('info_dashboard/', info_dashboard, name='info_dashboard'),
        path(f'db/{environ["BACKUP_TOKEN"]}/<filename>', download_db_file, name='download_db_file'),
        path('get_given_size/', get_given_size, name='get_given_size'),
        path('get_backups/', get_backups, name='get_backups'),
        path('do_backup/', do_backup, name='do_backup'),

        # --- seo ---
        path('seo/', seo, name='seo'),

        path('generate_robots/', generate_robots, name='generate_robots'),
        path('generate_sitemap/', generate_sitemap, name='generate_sitemap'),
        path('ping_google/', do_ping_google, name='ping_google_view'),
        path('generic/', generate_sitemap_and_ping_google, name='generic'),

        # --- utils ---
        path('utils/', UtilsView.as_view(), name='utils'),
        path('management_commands/', RunManagementCommandView.as_view(), name='management_commands'),
        path('used_components/', UsedComponentsView.as_view(), name='used_components'),
    ])),

    # --- seo ---
    path('sitemap.xml', get_sitemap, name='get_sitemap'),
    path('robots.txt', get_robots, name='get_robots'),
)
