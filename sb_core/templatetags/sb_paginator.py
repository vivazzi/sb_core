from math import floor

from django import template
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag(takes_context=True)
def show_paginator(context, objects, left_count=3, center_count=7, right_count=3, template_name='cms/paginator.html'):
    if center_count % 2 == 0: center_count += 1

    current_page_index = objects.number
    last_page_index = objects.paginator.num_pages

    left_range = range(1, left_count + 1)
    center_range = range(current_page_index - floor((center_count - 1) / 2),
                         current_page_index + floor((center_count - 1) / 2 + 1))
    right_range = range(last_page_index - right_count + 1, last_page_index + 1)

    # --- remove number intersection ---
    result = []
    for index, item in enumerate(center_range):
        if 0 < item <= last_page_index:
            result.append(center_range[index])
    center_range = result

    result = []
    for index, item in enumerate(left_range):
        if item not in center_range and 0 < item <= last_page_index:
            result.append(left_range[index])
    left_range = result

    result = []
    for index, item in enumerate(right_range):
        if item not in center_range and 0 < item <= last_page_index:
            result.append(right_range[index])
    right_range = result

    # --- concatenations ---
    page_range = left_range
    if left_range and left_range[-1] + 1 != center_range[0]:
        page_range += ['...', ]

    page_range += center_range

    if right_range and right_range[0] - 1 != center_range[-1]:
        page_range += ['...', ]

    page_range += right_range

    context.dicts[1]['objects'] = objects
    context.dicts[1]['page_range'] = page_range

    return render_to_string(template_name, context.dicts[1])


@register.simple_tag(takes_context=True)
def paginator(context, objects, per_page=10, page_index=None, par='page'):
    page_index = int(page_index) if page_index else context['request'].GET.get(par, 1)
    p = Paginator(objects, per_page)
    try:
        objects = p.page(page_index)
    except PageNotAnInteger:
        objects = p.page(1)
    except EmptyPage:
        objects = p.page(p.num_pages)

    objects.par = par
    return objects
