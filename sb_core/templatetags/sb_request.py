import urllib
from urllib.parse import urlencode

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def add_par_in_path(context, par, value, path=''):
    """
    Example: if path is None:
             - path = /some/path/?some_par=some_value
             <a href="{% add_par_in_path 'foo' 'bar' %}">Ссылка</a> render to
             <a href="/some/path/?some_par=some_value&foo=bar">Ссылка</a>

             if path is '':
             - path = /some/path/?some_par=some_value
             <a href="{% add_par_in_path 'foo' 'bar' '' %}">Ссылка</a> render to
             <a href="?some_par=some_value&foo=bar">Ссылка</a>

             if path:
             - path = /some/path/?some_par=some_value
             - another_path = /another/path/
             <a href="{% add_par_in_path 'foo' 'bar' another_path %}">Ссылка</a> render to
             <a href="/another/path/?some_par=some_value&foo=bar">Ссылка</a>

    Args:
        path:
        context:
        par: added GET parameter
        value: value of GET parameter

    Returns: reformed path with GET parameters
    """
    request = context['request']

    if path is None:
        path = request.path

    if par:
        qs_dict = request.GET.copy()
        qs_dict.update({par: value})
        return '{}?{}'.format(path, urlencode(qs_dict))

    return request.get_full_path()


@register.simple_tag(takes_context=True)
def del_par_in_path(context, *pars, **kwargs):
    """
    Example: if kwargs contains path parameter:
        if path is None:
             - path = /some/path/?some_par=some_value&foo=a&bar=b
             <a href="{% del_par_in_path 'foo' 'bar' %}">Ссылка</a> render to
             <a href="/some/path/?some_par=some_value">Ссылка</a>

             if path is '':
             - path = /some/path/?some_par=some_value&foo=a&bar=b
             <a href="{% del_par_in_path 'foo' 'bar' %}">Ссылка</a> render to
             <a href="?some_par=some_value">Ссылка</a>

             if path:
             - path = /some/path/?some_par=some_value&foo=a&bar=b
             - another_path = /another/path/
             <a href="{% del_par_in_path 'foo' 'bar' path=another_path %}">Ссылка</a> render to
             <a href="/another/path/?some_par=some_value">Ссылка</a>

    Args:
        path:
        context:
        pars: GET parameters for removing

    Returns: reformed path with GET parameters
    """
    request = context['request']

    path = kwargs.pop('path', '')

    if path is None:
        path = request.path

    qs_dict = request.GET.copy()

    for par in pars:
        if par in request.GET:
            del qs_dict[par]

    if qs_dict:
        return '{}?{}'.format(path, urlencode(qs_dict))

    return request.path


@register.simple_tag(takes_context=True)
def is_active_par(context, par, value):
    """
    Example: {% is_active_par par category.0 as is_active_category %}
             {% if is_active_category %}<a class="fa fa-times" href="{% del_par_in_path par %}" title="Сбросить фильтр">{% endif %}</a>

    Args:
        context:
        par: compared GET parameter
        value: value of GET parameter

    Returns: True if par equal value in request.GET otherwise False
    """
    request = context['request']
    return par and par in request.GET and request.GET.get(par) == value


@register.simple_tag(takes_context=True)
def select_active_par(context, par, value, active_class='active'):
    """
    Example: <a href="/path/to/page/" class="cat {% select_active_par 'gender' 'male' %}"> ... </a> render to
             <a href="/path/to/page/" class="category active"> ... </a>

    Args:
        context:
        par: compared GET parameter
        value: value of GET parameter
        active_class: 'active' by default

    Returns: active_class value if par equal value in request.GET
    """
    if is_active_par(context, par, value):
        return ' {}'.format(active_class)

    return ''


@register.simple_tag(takes_context=True)
def add_get_pars(context, *args):
    """
    Example: path = /some/path/?test_1=a&test_2=b&test_3=c
             <a href="{{ page.get_absolute_url }}{% add_get_pars 'test_1' 'test_2' %}"> ... </a> renders to
             <a href="path/to/page/?test_1=a&test_2=b"> ... </a>

    Args:
        context: context
        *args: needed GET parameters

    Returns: reformed path with GET parameters
    """
    request = context['request']

    qs_dict = {}
    for par in args:
        if par in request.GET:
            qs_dict.update({par: request.GET.get(par)})

    if qs_dict:
        return '?{}'.format(urlencode(qs_dict))

    return ''


@register.simple_tag(takes_context=True)
def get_without_pars(context, *args):
    """
    Example: path = /some/path/?color=a&test_2=b&test_3=c
    {% get_without_pars 'color' %} render to 'test_2=b&test_3=c'

    Args:
        context: context
        *args: GET parameters

    Returns: GET parameters without parameters in args
    """
    request = context['request']

    qs_dict = request.GET.copy()
    for par in args:
        if par in request.GET:
            del qs_dict[par]

    if qs_dict:
        return urlencode(qs_dict)

    return ''
