from django import template

register = template.Library()


@register.inclusion_tag('plugins/slider_detail.html', takes_context=True)
def slider_detail(context, pics, sample_pic=None, alt='', index=1, counter=1, pic_field='pic', use_zoom_slider=False, use_loading_icon=True,
                  product=None, obj_select=None):
    if not sample_pic:
        try:
            sample_pic = pics[0]
        except IndexError:
            sample_pic = None
    return {'request': context['request'],
            'SEKIZAI_CONTENT_HOLDER': context['SEKIZAI_CONTENT_HOLDER'],
            'STATIC_URL': context['STATIC_URL'],
            'pics': pics,
            'sample_pic': sample_pic,
            'alt': alt,
            'is_active': index == counter,
            'slider_data_id': counter,
            'pic_field': pic_field,
            'use_zoom_slider': use_zoom_slider,
            'use_loading_icon': use_loading_icon,
            'product': product, 'obj_select': obj_select}
