import urllib
from django import template

register = template.Library()


@register.simple_tag
def get_public_prev_sibling(page, need_public_page=False):
    if not need_public_page and page.publisher_is_draft:
        return page.node.get_prev_sibling()

    # we need public page
    node = page.node.get_prev_sibling()
    if node:
        page = node.get_item().publisher_public
        if page and page.title_set.all()[0].published:
            return node
        else:
            node = get_public_prev_sibling(node.get_item(), True)

    return node


@register.simple_tag()
def get_public_next_sibling(page, need_public_page=False):

    if not need_public_page and page.publisher_is_draft:
        return page.node.get_next_sibling()

    node = page.node.get_next_sibling()
    if node:
        page = node.get_item().publisher_public
        if page and page.title_set.all()[0].published:
            return node
        else:
            node = get_public_next_sibling(node.get_item(), True)

    return node

