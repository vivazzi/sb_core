from classytags.arguments import Argument, MultiKeywordArgument
from classytags.core import Tag, Options
from cms.templatetags.cms_tags import MultiValueArgumentBeforeKeywordArgument
from django import template
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.template.defaultfilters import safe
from os.path import exists, join

from django.template.loader import render_to_string

from sb_core.fields import PicField, SvgImageFieldFile
from sb_core.templatetags.sb_math_tags import to_percent
from sb_core.utils.image import get_no_image_url
from sb_core.utils.thumbnail import thumb, retina, svg

register = template.Library()


def retina_2(obj, size, alt='', class_='', crop_type=True, is_response=True, pic_field='pic', silent=False, tag='div',
             is_retina=None, **kwargs):
    context = {}
    w, h = size.split('x')
    w = int(w)
    h = int(h)

    obj = getattr(obj, pic_field, None)
    is_obj_exists = bool(obj) and exists(obj.path)
    if not is_obj_exists and silent:
        return ''

    context.update({'obj': obj,
                    'is_response': is_response,
                    'alt': alt,
                    'size': (size, 'x'.join([str(v * 2) for v in (w, h)])),
                    'tag': tag})

    if crop_type == 'save_height' and exists(obj.path):
        ar = float(w) / h

        obj_w, obj_h = obj.width, obj.height
        obj_ar = float(obj_w) / obj_h

        crop_type = ar < obj_ar

    classes = class_.split()

    if is_response: classes.append('img-responsive')
    if not crop_type: classes.append('center-block')

    context['class'] = ' '.join(classes)
    context['crop_type'] = crop_type

    context.update(kwargs)

    if not is_obj_exists:
        if 'no_image_url' in context:
            context['no_image_url'] = join(settings.STATIC_URL, context['no_image_url'])
        else:
            context['no_image_url'] = get_no_image_url(w, h)
        context['bad_url'] = getattr(obj, 'url', None) if obj else 'undefined'
        return render_to_string('retina/_no_image_2.html', context)

    # padding_top = to_percent(obj.instance.get_height() / obj.instance.get_width())
    style = kwargs.pop('style', '')
    # style += 'padding-top:{};max-width:{}px;'.format(padding_top, obj.width)
    # style += 'max-width:{}px;'.format(obj.width)

    content_type = ContentType.objects.get_for_model(obj.field.model)
    context.update({'content_type_id': content_type.id,
                    'object_id': obj.instance.id,
                    'pic_field': pic_field,
                    'is_retina': is_retina,
                    'style': style,
                    # 'padding_top': padding_top,
                    })

    return render_to_string('retina/retina_2.html', context)


@register.simple_tag(name='retina')
def do_retina(obj, w=0, h=0, title='', th_type='cover', fluid=False, tag='div', is_retina=None, postfix=None, **kwargs):
    if isinstance(obj, str):
        return ''

    pic_field = kwargs.pop('pic', 'pic')

    if isinstance(getattr(obj, pic_field), SvgImageFieldFile) and getattr(obj, pic_field).is_svg():
        return do_svg(obj, w, h, title, style=kwargs.get('style', ''), pic_field=pic_field)

    if not w: w = 0
    if not h: h = 0

    if tag == 'img':
        size = 'x'.join([str(w), str(h)])
        class_ = kwargs.pop('th_class', 'th')
        is_response = kwargs.pop('is_response', True)
        silent = kwargs.pop('silent', False)

        if th_type == 'cover':
            crop_type = True
        elif th_type == 'contain':
            crop_type = False
        else:
            crop_type = th_type

        if is_retina is None:
            is_retina = True

        return retina_2(obj, size, alt=title, class_=class_, crop_type=crop_type, is_response=is_response,
                        pic_field=pic_field, silent=silent, tag=tag, is_retina=is_retina, **kwargs)

    return retina(obj, w, h, postfix, title, th_type, fluid, tag, is_retina, pic_field=pic_field, **kwargs)


class Retina(Tag):
    name = 'retina_block'
    options = Options(
        Argument('to', resolve=False),
        MultiValueArgumentBeforeKeywordArgument('args'),
        MultiKeywordArgument('kwargs', required=False),
        blocks=[('end_retina_block', 'nodelist')],
    )

    def render_tag(self, context, to, args, kwargs, nodelist):
        output = nodelist.render(context)
        k = 'content_wr' if to == 'to_wr' else 'content_th'
        kwargs.update({k: output})
        return do_retina(*args, **kwargs)

register.tag('retina_block', Retina)


@register.simple_tag(name='thumb')
def do_thumb(pic, width=0, height=0, th_type='cover', postfix=None):
    if postfix is None:
        postfix = '_{}x{}'.format(width, height)
    return thumb(pic, width, height, th_type, postfix)


@register.simple_tag
def get_max_parameters(pic_width, pic_height, thumbnail_width=None, thumbnail_height=None):
    max_width = thumbnail_width if thumbnail_width is not None else pic_width
    max_height = thumbnail_height if thumbnail_height is not None else pic_height
    html = ''
    html += 'max-width: {}px;'.format(max_width) if max_width else ''
    html += 'max-height: {}px;'.format(max_height) if max_height else ''
    return safe(html)


@register.simple_tag(name='svg')
def do_svg(obj, w, h=None, hint='', style='', pic_field='pic'):
    return svg(obj, w, h, hint, style, pic_field)
