from django import template
from sb_core import settings

register = template.Library()


@register.simple_tag
def default_url(lib):
    data = {'jquery': settings.JQUERY_URL,
            'awesome': settings.AWESOME_URL,
            'angular': settings.ANGULAR_URL}
    return data[lib]
