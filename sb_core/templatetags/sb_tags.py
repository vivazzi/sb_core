import json
import os
from datetime import datetime
from itertools import chain
from urllib.parse import urlsplit, urljoin

import bleach
import sys

from bleach import Linker

from sb_core.settings import PROTOCOL

try:
    from cms.templatetags.cms_tags import render_plugin
except ImportError:
    render_plugin = None

from django import template
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.forms import FileField, BooleanField
from django.template import TemplateDoesNotExist
from django.template.loader import get_template
from django.utils.html import format_html, linebreaks
from django.utils.safestring import mark_safe
from django.utils.text import Truncator
from sb_core.constants import ALLOWED_TAGS, DIGITS
from sb_core.utils.image import get_no_image_url, get_dummy_pic
from sb_core.utils.sb_utils import get_phone, bool_py_to_js, with_end, separate_thousand
from django.contrib.sites.shortcuts import get_current_site
from django.utils.translation import get_language
from django.contrib import admin

register = template.Library()


@register.filter(name='format')
def do_format(v, obj):
    return v.format(obj)


@register.filter
def filter_plugins(all_plugins, required_plugin_name):
    if all_plugins:
        return [plugin.get_plugin_instance()[0] for plugin in all_plugins if type(plugin.get_plugin_instance()[1]).value == required_plugin_name]

    return []


@register.simple_tag
def get_project_years(start_year, separate='−'):
    current_year = datetime.now().year

    if start_year < current_year:
        return f'{start_year}{separate}{current_year}'

    return start_year


@register.filter(is_safe=False)
def default_if_empty(value, arg):
    if value is None or value == '':
        return arg
    return value


@register.filter(name='bool')
def do_bool(value):
    return bool(value)


@register.filter
def revert_bool(value):
    if type(value) is bool:
        return not value
    return value


@register.filter
def add_if_not_empty(value, arg):
    if value is None or value == '':
        return value
    return value + arg


@register.filter(is_safe=False)
def px_default(value, arg):
    if value is None or value == '':
        return arg
    return '{}px'.format(value)


@register.filter(name='bool_py_to_js')
def do_bool_py_to_js(value):
    return bool_py_to_js(value)


@register.filter
def is_equal(v_1, v_2):
    return v_1 == v_2


@register.filter(name='separate_thousand')
def do_separate_thousand(value):
    return separate_thousand(value)


@register.simple_tag
def is_debug():
    return settings.DEBUG


@register.filter
def get_fields(obj):
    return obj._meta.fields


@register.filter
def get_value_from_field(obj, attname):
    if hasattr(obj, 'form') and hasattr(obj.form, 'get_{}_display'.format(attname)):
        return getattr(obj.form, 'get_{}_display'.format(attname))(obj.value())

    if hasattr(obj, 'value'):
        if attname == 'phone':
            return get_phone(obj.value())

        if hasattr(obj, 'field') and type(obj.field) == BooleanField:
            return 'Да' if obj.value() else 'Нет'

        return obj.value()

    return getattr(obj, attname, None)


@register.filter
def get(dictionary, key):
    try:
        return dictionary.get(key, None)
    except AttributeError:
        return ''


@register.filter
def get_key_by_value(dictionary, value):
    try:
        for k, v in dictionary.items():
            if v == value: return k
    except AttributeError:
        pass

    return None


@register.filter
def get_attr(value, attr):
    return getattr(value, attr, None)


@register.simple_tag
def rjust(value, min_count=3, fill_char=' '):
    return value.rjust(min_count, fill_char)


@register.simple_tag(name='company_name')
def get_company_name(is_short=False):
    company_name = 'SHORT_COMPANY_NAME' if is_short else 'COMPANY_NAME'
    return getattr(settings, company_name)


@register.filter
def is_redirect_to_404(value):
    if value:
        value = value.replace('/', '').replace('\\', '')
        return value == '404'

    return False


@register.filter
def not_more(value, max_value):
    if value and value < max_value: return value
    else: return max_value


@register.filter
def exists(path, file_type=None):
    if file_type == 'template':
        try:
            get_template(path)
            return True
        except TemplateDoesNotExist:
            return False
    return os.path.exists(path)


@register.simple_tag
def calc_border_radius(pic):
    return max(pic.width, pic.height)


@register.simple_tag(name='parse_url')
def parse_url_tag(url, part='host'):
    protocol, host, path, query, fragment = list(urlsplit(url))
    part_dict = {'protocol': protocol,
                 'host': host,
                 'path': path,
                 'query': query,
                 'fragment': fragment}
    return part_dict.get(part, '')


@register.filter(name='parse_url')
def parse_url_filter(url, part='host'):
    return parse_url_tag(url, part)


# --- SITE TAGS ---
@register.simple_tag
def get_site_name():
    return get_current_site(None).name


@register.simple_tag
def get_domain(with_protocol=True, with_slash=False):
    protocol = '{}://'.format(PROTOCOL) if with_protocol else ''
    slash = '/' if with_slash else ''
    return ''.join([protocol, get_current_site(None).domain, slash])


@register.simple_tag
def get_domain_with_protocol(with_slash=True):
    sys.stderr.write('[WARNING] get_domain_with_protocol() is deprecated method\n')
    return get_domain(True, with_slash)


@register.filter
def with_domain(url):
    return urljoin(get_domain(True, False), url)
# ------------------


@register.simple_tag
def get_admin_site_title():
    return admin.site.site_title


@register.filter
def is_file_field(field):
    return type(field.field) == FileField


@register.filter
def is_boolean_field(field):
    return type(field.field) == BooleanField


@register.filter
def has_file_field(form):
    return any([is_file_field(field) for field in form])


@register.filter
def get_url(pic):
    try:
        return pic.url
    except (ValueError, AttributeError):
        return 'url is not undefined'


@register.simple_tag()
def no_image_url(w, h):
    return get_no_image_url(w, h)


@register.simple_tag(takes_context=True)
def find_text_from_page(context, page, words=30, use_truncate_as_link=False, truncate=' ...',
                        find_in_plugin_types=None):
    """

    :param context:
    :param page:
    :param words:
    :param use_truncate_as_link:
    :param truncate:
    :param find_in_plugin_types: separated by space,
           Ex: find_in_plugin_types='TextPlugin SBSimpleTextPlugin SBSnippetPlugin'
    :return:
    """
    def truncatewords_html(value, arg, truncate):
        try:
            length = int(arg)
        except ValueError:
            return value  # Fail silently.
        return Truncator(value).words(length, html=True, truncate=truncate)

    language = get_language()
    if hasattr(page, 'pageext') and hasattr(page.pageext, 'short_desc') and page.pageext.short_desc:
        return mark_safe(linebreaks(page.pageext.short_desc))

    if use_truncate_as_link:
        truncate = ' <a href="{}" class="truncate a_i" title="Перейти на страницу &#8243;{}&#8243;">{}</a>'.format(
            page.get_absolute_url(), page.get_title(), truncate)

    find_in_plugin_types = find_in_plugin_types.split() if find_in_plugin_types else ('TextPlugin', )

    for placeholder in page.placeholders.all():
        for plugin in placeholder.get_plugins(language):
            if plugin.plugin_type in find_in_plugin_types:
                if plugin.plugin_type == 'TextPlugin':
                    text = getattr(plugin, 'djangocms_text_ckeditor_text', getattr(plugin, 'text', None))
                    return mark_safe(truncatewords_html(text.body, words, truncate)) if text else ''
                if plugin.plugin_type == 'SBSimpleTextPlugin':
                    return mark_safe(truncatewords_html(plugin.sb_simple_text_sbsimpletext.content, words, truncate))

                if plugin.plugin_type == 'SBSnippetPlugin':
                    if render_plugin:
                        return mark_safe(render_plugin(context, plugin))

                    return mark_safe(plugin.render_plugin())

    return ''


@register.filter
def clean_tags(value):
    return bleach.clean(value, tags=ALLOWED_TAGS, strip=True)


@register.filter
def linkify(value, use_target=True):
    def set_target(attrs, new=False):
        attrs[(None, 'target')] = '_blank'
        return attrs

    callbacks = []
    if use_target:
        callbacks.append(set_target)

    linker = Linker(callbacks=callbacks)
    return mark_safe(linker.linkify(value))


@register.filter(name='call')
def call_method(obj, method_name):
    method = getattr(obj, method_name)

    if hasattr(obj, '__callArg'):
        ret = method(*obj.__callArg)
        del obj.__callArg
        return ret
    return method()


@register.filter
def args(obj, arg):
    if not hasattr(obj, '__callArg'): obj.__callArg = []
    obj.__callArg += [arg]
    return obj


@register.filter
def update(dictionary, value):
    if not dictionary:
        dictionary = {}

    splits = value.split(':')
    if len(splits) == 2:
        dictionary.update({splits[0]: splits[1]})
        return dictionary
    raise ValueError('use "key:value"')


@register.filter
def load_dict(value):
    return json.loads(value)


# @register.simple_tag
# def get_next_page(page):
#     if cms.__version__ == '3.5.0':
#         return page.node.get_next_sibling().get_item() if page.node.get_next_sibling() else None
#
#     # workaround fixes bug with display get_next_sibling if page.depth == 1
#     if page.depth == 1 and page.get_next_sibling():
#         return page.get_next_sibling().get_next_sibling()
#
#     return page.get_next_sibling()


@register.filter
def get_dir(obj):
    return dir(obj)


@register.filter
def split_and_get(value, string):
    """
        First char is separator. Next chars are index for getting

        Examples:
         - in template: 'string_1, string_2'|split_and_get:',1'
         - in python code: split_and_get('string_1, string_2', ',1')
        Returns: string_2
    """
    sep = string[0]
    index = string[1:]
    v = value.split(sep)
    return v[index]


@register.filter(name='in_list')
def check_element_in_list(value, array):
    """
        Usage:
          checked_title|in_list:'[string_1][sep][string_2][sep]..[string_N];[sep]'

        Examples:
         - title|in_list:'string_1,string_2'
         - title|in_list:'string_1,string_2;,'  # with separate ','
        Returns: True or False
    """
    sep = ','  # by default
    pars = array.split(';')

    if len(pars) == 2:
        sep = pars[1]

    return value in pars[0].split(sep)


@register.filter(name='with_end')
def do_with_end(number, array):
    """
        Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
        number int - Число на основе которого нужно сформировать окончание
        Array Массив слов, разделяющиеся между собой пробелами, например 'яблоко_яблока_яблок'
        (1 яблоко, 2 яблока, много яблок -> 'яблоко_яблока_яблок')

        Ex: <span>{{ count }}</span> {{ count|with_end:"яблоко_яблока_яблок;False" }}
    """
    show_number = True
    pars = array.split(';')
    if len(pars) == 2:
        array, show_number = pars

        if show_number == 'False': show_number = False
        elif show_number == 'True': show_number = True
        else: raise Exception('You can use "True" or "False" values in #"show_number" parameter')

    return with_end(number, array, show_number)


@register.simple_tag
def theming_url():
    return getattr(settings, 'ADMIN_TOOLS_THEMING_CSS', staticfiles_storage.url('/sb_core/css/theming.css'))


@register.simple_tag
def admin_favicon_url():
    return getattr(settings, 'ADMIN_FAVICON_URL', staticfiles_storage.url('/sb_core/images/admin_favicon.svg'))


@register.filter
def order_by(objects, order_by_value):
    if objects:
        return objects.order_by(order_by_value)

    return objects


@register.filter
def perform_phone(v, mode='+X (XXXX) XXX-XXX'):
    use_plus = mode.find('+X') != -1
    mode = mode.replace('X', '{}')
    p = ''
    for w in v:
        if w in DIGITS: p += w

    if use_plus and p:
        p = '{}{}'.format(int(p[0]) - 1, p[1:])

    try:
        return mark_safe(mode.format(*p))
    except IndexError:
        return mark_safe('Ошибка. Проверьте, что значение "{}" используется в правильном формате.'.format(v))


@register.simple_tag
def generate_dummy_pic_url(title, color='#addea5', w=800, h=600, save=False, path='dummy_pics', use_size_as_title=False):
    resource, url = get_dummy_pic(title, color, w, h, save, path, use_size_as_title)
    return url


@register.filter
def media_files(form, mode):
    if mode == 'js':
        return [format_html('<script src="{}"></script>', form.media.absolute_path(path)) for path in getattr(form.media, '_js')]

    if mode == 'css':
        media = sorted(form.media._css.keys())
        return chain(*[[
            format_html(
                '<link rel="stylesheet" href="{}" />',
                form.media.absolute_path(path), medium
            ) for path in form.media._css[medium]
        ] for medium in media])

    raise Exception('You can choose: js or css for mode argument in media_files() filter')


@register.filter
def decode(v, encoding='utf-8'):
    """
        For converting bytes to str
    """
    return v.decode(encoding)


@register.simple_tag(takes_context=True)
def get_block_name(context):
    if 'structure' not in context['request'].GET:
        return 'cms_js'
    return 'js'
