
from decimal import Decimal
from django import template
from sb_core.utils.sb_utils import convert_to_point

register = template.Library()


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter
def multiply(value, arg):
    return float(value) * float(arg)


@register.filter
def division(value, divider):
    return float(value) / float(divider)


@register.filter(name='convert_to_point')
def convert_to_point_tag(value):
    return convert_to_point(value)


@register.filter
def mod(value, divider):
    return value % divider


@register.filter
def div(value, divider):
    return int(value) / int(divider)


@register.filter(name='abs')
def do_abs(value):
    return abs(float(value))


@register.filter(name='round')
def do_round(value, precision=0):
    if type(value) == str:
        value = Decimal(value.replace(',', '.'))
    return round(value, precision)


@register.filter(name='int')
def do_int(value):
    return int(value)


@register.filter(name='float')
def do_float(value):
    return float(value)


@register.filter
def without_zero(value):
    if value is None:
        return ''

    value = str(value)

    if value == '0':
        return value

    value = value.replace(',', '.')
    if '.' in value:
        value = value.strip('0')

        if value[0] in ('.', ','):
            value = '0{}'.format(value)

        if value[-1] in ('.', ','):
            value = value[:-1]

    return value


@register.filter(name='range')
def do_range(value, start_index=0):
    return range(start_index, value + start_index)


@register.filter
def to_percent(value, precision=0):
    precision = int(precision)
    value = float(value)
    return '{}%'.format(do_round(value * 100, precision))


@register.filter
def l_and(v_1, v_2):
    return v_1 and v_2


@register.filter
def l_or(v_1, v_2):
    return v_1 or v_2
