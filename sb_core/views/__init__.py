from .errs import bad_request, permission_denied, page_not_found, server_error
from .utils import UtilsView, RunManagementCommandView, UsedComponentsView
from .info_dashboard_views import info_dashboard, get_backups, download_db_file, do_backup, get_given_size
from .seo_views import (seo, generate_sitemap, do_ping_google, generate_sitemap_and_ping_google, get_robots,
                        get_sitemap, generate_robots, generate_sitemap_handler, my_ping_google)
