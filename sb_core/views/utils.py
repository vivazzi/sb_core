import io
import sys
from cms.models import CMSPlugin
from cms.plugin_pool import plugin_pool
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.core import management
from django.core.management import CommandError
from django.contrib import messages

from sb_core.forms import RunManagementCommandForm
from sb_core.utils.sb_utils import humanize_bytes, send_mails_if_debug, str_to_args_for_management_commands

try:
    from post_office import mail
except ImportError:
    mail = None
from django.utils.translation import gettext as _


def remove_nbsp():
    from djangocms_text_ckeditor.models import Text
    total = 0

    plugins = Text.objects.filter(body__icontains=' ')
    for plugin in plugins:
        total += plugin.body.count(' ')
        plugin.body = plugin.body.replace(' ', ' ')
        plugin.save()

    return total


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class UtilsView(View):
    template_name = 'admin/sb_core/utils.html'

    @staticmethod
    def common_ctx(request):
        return {
            'title': 'Утилиты сайта',
            'email': request.user.email,
        }

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.common_ctx(request))

    def post(self, request, *args, **kwargs):
        ctx = self.common_ctx(request)

        if 'method' in request.POST:
            if 'accelerate' in request.POST['method']:
                accelerated = False
                free_bites = remove_nbsp()
                if free_bites:
                    free_bites = humanize_bytes(free_bites)
                    ctx.update({'free_bites': free_bites})
                    accelerated = True

                ctx.update({'method': 'accelerate', 'accelerated': accelerated})

            if 'send_test_email' in request.POST['method']:
                me = '{} <{}>'.format(settings.SHORT_COMPANY_NAME, settings.EMAIL_HOST_USER)
                subject = 'Test message'

                message = _('<p><strong>Test html message. This text must be bold</strong>.<br/>'
                            'If it is true, then letter was received correctly.</p>')

                headers = {'To': f'Получатель тестового письма {request.user.get_full_name()} <{request.user.email}>'}
                if settings.EMAIL_BACKEND == 'post_office.EmailBackend':
                    mail.send(request.user.email, me, subject=subject, html_message=message, headers=headers)
                    send_mails_if_debug()
                else:
                    email = EmailMessage(subject, message, me, (request.user.email,), headers=headers)
                    email.content_subtype = 'html'
                    email.send()

                ctx.update({'method': 'send_test_email', 'status': 'ok'})

        return render(request, self.template_name, ctx)


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class RunManagementCommandView(View):
    template_name = 'admin/sb_core/management_commands.html'
    form = RunManagementCommandForm

    @property
    def command_help_list(self):
        res = []
        for command, app in management.get_commands().items():
            try:
                command_class = management.load_command_class(app, command)
                res.append(
                    (app, command, command_class.create_parser('', '{} (of {} app)'.format(command, app)).format_help())
                )
            except ImportError as e:
                sys.stderr.write('[WARNING] {} | command "{}" (of {} app)\n'.format(e, command, app))
        return res

    def common_ctx(self):
        return {
            'title': 'Выполнение команды',
            'commands': self.command_help_list,
        }

    def get(self, request, *args, **kwargs):
        ctx = self.common_ctx()
        ctx['form'] = self.form()
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = self.common_ctx()
        form = self.form(request.POST)

        if form.is_valid():
            parts = form.cleaned_data['command'].split('.')
            command = parts[-1]
            try:
                out = io.StringIO()
                management.call_command(command, stdout=out,
                                        *str_to_args_for_management_commands(form.cleaned_data['params']))
                messages.success(request, 'Команда "{}" выполнена'.format(command))

                ctx['result'] = out.getvalue()
            except CommandError as mes:
                messages.error(request, mes)
            except SystemExit:
                messages.info(request, 'Команда "{}" выполнена со стандартной опцией'.format(command))

        ctx['form'] = form
        return render(request, self.template_name, ctx)


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class UsedComponentsView(View):
    template_name = 'admin/sb_core/used_components.html'

    def get(self, request, *args, **kwargs):
        components = {}
        for plugin_name in plugin_pool.plugins.keys():
            plugins = CMSPlugin.objects.filter(plugin_type=plugin_name)
            pages = set()
            for plugin in plugins:
                if plugin.page:
                    if plugin.page.publisher_is_draft:
                        pages.add((plugin.page.get_title(), plugin.page.get_absolute_url()))
                else:
                    slot_name = plugin.placeholder.slot
                    if slot_name == 'clipboard':
                        pages.add(('В буфере обмена', None))
                        continue

                    if plugin.placeholder.slot in settings.CMS_PLACEHOLDER_CONF:
                        slot_name = settings.CMS_PLACEHOLDER_CONF[slot_name].get('name', slot_name)
                    pages.add(('В статическом заполнителе "{}".'.format(slot_name), None))
            components[plugin_name] = pages

        return render(request, self.template_name, {
            'title': 'Используемые компоненты на страницах',
            'components': components
        })
