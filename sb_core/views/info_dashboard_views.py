import json
import os
from datetime import datetime
from os import listdir
from os.path import join, exists, basename, getmtime

import requests
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.http import JsonResponse, FileResponse, Http404
from django.shortcuts import render
from django.urls import reverse

from django.views.decorators.http import require_GET

from sb_core.constants import NEED_BACKUP, LOCKFILE
from sb_core.settings import BACKUP_INTERVAL, BACKUP_COUNT, DB_DIR
from sb_core.utils.sb_utils import custom_convert_seconds, convert_seconds, get_stat


@user_passes_test(lambda u: u.is_superuser)
def info_dashboard(request):
    stat = get_stat()

    context = {
        'title': 'Статистика и резервные копии',
        'backup_interval': BACKUP_INTERVAL,
        'backup_interval_str': convert_seconds(BACKUP_INTERVAL),
        'backup_count': BACKUP_COUNT,
        'stat': json.dumps(stat.data),
    }

    return render(request, 'admin/sb_core/info_dashboard.html', context)


@user_passes_test(lambda u: u.is_superuser)
def get_backups(request):
    current_elapsed_time = 0

    stat = get_stat()

    lockfile_content = ''
    lockfile_path = DB_DIR / LOCKFILE
    if exists(lockfile_path):
        with open(lockfile_path) as f:
            lockfile_content = f.read().strip()

        if lockfile_content != NEED_BACKUP:
            current_elapsed_time = custom_convert_seconds(datetime.now().timestamp() - getmtime(lockfile_path))

    for filename in listdir(DB_DIR):
        if filename in stat.data['sizes']['backups']['files']:
            stat.data['sizes']['backups']['files'][filename]['url'] = reverse('download_db_file', args=(filename,))

    return JsonResponse({
        'lockfile_content': lockfile_content,
        'status': 'OK',

        'backup_interval': BACKUP_INTERVAL,
        'backup_interval_str': convert_seconds(BACKUP_INTERVAL),
        'backup_count': BACKUP_COUNT,
        'stat': stat.data,

        'current_elapsed_time': current_elapsed_time,
    })


@user_passes_test(lambda u: u.is_superuser)
def download_db_file(request, filename):
    # noinspection GrazieInspection
    filename = basename(filename)  # it helps exclude bad urls such as '../some_path/' and so on

    file_path = DB_DIR / filename
    if not exists(file_path):
        raise Http404(f'File "{file_path}" is not found')

    response = FileResponse(open(file_path, 'rb'), content_type='application/x-gzip')
    response['Content-Disposition'] = f'attachment; filename={filename}'

    return response


@user_passes_test(lambda u: u.is_superuser)
def do_backup(request):
    lockfile_content = ''
    lockfile_path = join(DB_DIR, LOCKFILE)
    if not exists(lockfile_path):
        lockfile_content = NEED_BACKUP
        with open(lockfile_path, 'w', encoding='utf-8') as f:
            f.write(lockfile_content)

    return JsonResponse({'status': 'OK', 'lockfile_content': lockfile_content})


@require_GET
def get_given_size(request):
    if settings.DEBUG:
        return JsonResponse({'status': 'OK', 'size': 1073741824})  # 1 GB

    token = getattr(settings, 'VITS_TOKEN', None)
    if not token:
        return JsonResponse({'status': 'fail', 'mes': 'There is no VITS_TOKEN in settings'})

    try:
        r = requests.get(f'https://vits.pro/get_given_size/{basename(settings.BASE_DIR)}?token={token}')
    except Exception as e:
        return JsonResponse({'status': 'fail', 'mes': f'ERROR: {str(e).replace(token, "***")}'})

    if r.status_code != 200:
        return JsonResponse({'status': 'fail', 'mes': 'Vuspace service is unavailable'})

    return JsonResponse({'status': 'OK', 'size': r.text})
