import os
import datetime
import xml.dom.minidom

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.contrib.sitemaps import ping_google, Sitemap
from django.contrib.sitemaps.views import sitemap
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import mail_admins
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse

from sb_core.constants import ROBOTS_PATH, SITEMAP_PATH
from sb_core.templatetags.sb_tags import with_domain

try:
    from sitemaps import STATIC_URLS
except ImportError:
    STATIC_URLS = {}

try:
    from sitemaps import SITEMAPS
except ImportError:
    SITEMAPS = {}


class AbstractSitemapClass():
    changefreq = 'weekly'
    url = None

    def get_absolute_url(self):
        return self.url


class StaticSitemap(Sitemap):
    main_sitemaps = []
    for page in STATIC_URLS.keys():
        sitemap_class = AbstractSitemapClass()
        sitemap_class.url = STATIC_URLS[page]
        main_sitemaps.append(sitemap_class)

    def items(self):
        return self.main_sitemaps

    now_date = datetime.date.today()

    lastmod = datetime.datetime(now_date.year, now_date.month, now_date.day)
    priority = 0.5
    changefreq = 'weekly'


# --- for management commands (not included in urls.py) ---
def generate_sitemap_handler(request):
    if STATIC_URLS:
        SITEMAPS.update({'static_urls': StaticSitemap})

    with open(SITEMAP_PATH, 'w', encoding='utf-8') as f:
        if SITEMAPS:
            xml = sitemap(request, SITEMAPS)
            f.write(xml.rendered_content)


def my_ping_google(file_name='/sitemap.xml'):
    ping_google(file_name)


# --- private views ---
@user_passes_test(lambda u: u.is_staff)
def seo(request):
    # noinspection PyBroadException
    try:
        dom = xml.dom.minidom.parseString(_get_sitemap_content(request))
        sitemap_content = dom.toprettyxml()
    except Exception:
        sitemap_content = 'Пусто'

    ctx = {
        'title': 'SEO',
        'robots': {
            'url': with_domain(reverse('get_robots')),
            'content': _get_robots_content(),
        },
        'default_robots_content': default_robots_content(),
        'sitemap': {
            'url': with_domain(reverse('get_sitemap')),
            'content': sitemap_content,
        },
    }

    return render(request, 'admin/sb_core/seo.html', ctx)


# todo: maybe generate_sitemap is useless
def generate_sitemap(request):
    if not request.user.is_superuser:
        return HttpResponse('You do not have access')

    # try:
    generate_sitemap_handler(request)
    messages.success(request, 'Сгенерирована карта сайта')
    return redirect('/')
    # except Exception:
    #     return HttpResponse('Error "generate sitemap"')


# todo: maybe do_ping_google is useless
def do_ping_google(request):
    if not request.user.is_superuser:
        return HttpResponse('You do not have access')

    if settings.DEBUG:
        return HttpResponse('DEBUG is True. This function does not work in debug mode.')

    try:
        my_ping_google()
        messages.success(request, 'Успешно оповещены поисковые системы о сайте')
        return redirect(reverse('admin:index'))
    except Exception:
        return HttpResponse('Error "ping google"')


def generate_sitemap_and_ping_google(request):
    if not request.user.is_staff:
        return HttpResponse('You do not have access')

    generate_sitemap_handler(request)
    if settings.DEBUG:
        mes = 'Сгенерирована карта сайта (без оповещения в поисковые системы, так как сайт запущен в режиме DEBUG)'
    else:
        my_ping_google()
        mes = 'Сгенерирована карта сайта и оповещены поисковые системы о сайте'

    messages.success(request, mes)

    if request.META.get('HTTP_REFERER'):
        return redirect(request.META.get('HTTP_REFERER'))

    return redirect('/')


# --- public views ---
def default_robots_content():
    current_site = get_current_site(None)
    extra_content = {'host': current_site.domain,
                     'protocol': getattr(settings, 'HTTP_PROTOCOL', 'https')}
    return render_to_string('robots.txt', extra_content)


def generate_default_robots_content():
    with open(ROBOTS_PATH, 'w', encoding='utf-8') as f:
        f.write(default_robots_content())


def generate_robots(request):
    if not request.user.is_staff:
        return HttpResponse('You do not have access')

    generate_default_robots_content()
    messages.success(request, 'Сгенерирован robots.txt')

    if request.META.get('HTTP_REFERER'):
        return redirect(request.META.get('HTTP_REFERER'))

    return redirect(reverse('seo'))


def _get_robots_content():
    if not os.path.exists(ROBOTS_PATH):
        generate_default_robots_content()

    with open(ROBOTS_PATH, encoding='utf-8') as f:
        return f.read()


def get_robots(request):
    return HttpResponse(_get_robots_content(), content_type='text/plain')


def _get_sitemap_content(request):
    if not os.path.exists(SITEMAP_PATH):
        generate_sitemap_handler(request)
        mes = 'Сгенерирован sitemap.xml у сайта "{}"'.format(get_current_site(None))
        mail_admins(mes, '{}. Раньше этого файла не было, либо он был удалён'.format(mes))

    with open(SITEMAP_PATH, encoding='utf-8') as f:
        return f.read()


def get_sitemap(request):
    return HttpResponse(_get_sitemap_content(request), content_type='application/xml')
