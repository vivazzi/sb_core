from cms.extensions import PageExtension
from cms.models import CMSPlugin
from sb_core.models import _copy_relations_handler, FileModel, PicModel


class SBCMSPluginMixin(object):
    def copy_relations(self, old_instance):
        _copy_relations_handler(self, old_instance)


class SBPageExtensionMixin(object):
    def copy_relations(self, old_instance, language):
        _copy_relations_handler(self, old_instance)


class FileModelPlugin(SBCMSPluginMixin, CMSPlugin, FileModel):
    class Meta:
        abstract = True


class PicModelPlugin(SBCMSPluginMixin, CMSPlugin, PicModel):
    class Meta:
        abstract = True


class FileModelExtension(SBPageExtensionMixin, PageExtension, FileModel):
    class Meta:
        abstract = True


class PicModelExtension(SBPageExtensionMixin, PageExtension, PicModel):
    class Meta:
        abstract = True
