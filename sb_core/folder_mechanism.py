import os
import shutil
from django.conf import settings
from sb_core.utils.sb_utils import get_random_seq
from os.path import exists, join, basename, dirname

THUMBNAIL_PART = 'thumb_'


def upload_to_handler(instance, filename):
    folder = instance.folder
    if hasattr(instance, 'file_fields') and len(instance.file_fields) >= 2:
        is_exists = True
        i = 0
        while is_exists:
            folder = '{}_{}'.format(instance.folder, i)
            is_exists = exists(join(settings.MEDIA_ROOT, instance.upload_to, folder))
            i += 1
    return join(instance.upload_to, folder, filename)


def set_new_folder(root, new_folder):
    _root = dirname(root)
    os.rename(root, join(_root, new_folder))


def generate_folder(root, length=5):
    folder = ''
    is_exists = True

    while is_exists:
        folder = get_random_seq(length)
        new_path = join(root, folder)
        is_exists = exists(new_path)

    return folder


def delete_folder(file_path, folder):
    file_dir = dirname(file_path)
    if basename(file_dir) == folder and exists(file_dir):
        shutil.rmtree(file_dir)


def copy_file_changing_folder(f, folder, f_postfix=''):
    if f_postfix:
        folder = '{}_{}'.format(folder, f_postfix)
    new_dir = join(settings.MEDIA_ROOT, f.instance.upload_to, folder)
    new_path = join(new_dir, basename(f.path))

    if exists(f.path):
        if not exists(new_dir):
            os.makedirs(new_dir)
        shutil.copy(f.path, new_path)

    return join(f.instance.upload_to, folder, basename(f.name))


def change_folder(path, reverse=False):
    """
     It is needs to do not remove files when execute save()
    """
    root_with_folder = dirname(path)
    if root_with_folder != settings.MEDIA_ROOT:
        folder = basename(root_with_folder)
        root = dirname(root_with_folder)
        if not reverse:
            if exists(root_with_folder):
                os.rename(root_with_folder, join(root, 'temp_{}'.format(folder)))
        else:
            if exists(join(root, 'temp_{}'.format(folder))):
                os.rename(join(root, 'temp_{}'.format(folder)), join(root, folder))
