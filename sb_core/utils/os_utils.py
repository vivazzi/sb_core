import ctypes
from datetime import datetime
import os
import platform
import re
import tempfile
from os.path import isfile


def delete_old_files(path, seconds, f_out=None, log_path=None, pattern=None):
    """
    If select f_out then this file will be use as logfile and will not be closed.
    If select log_path then file will be open by path and will be closed.
    """
    if all([f_out, log_path]): f_out.write('{}: choose f_out or log_path'.format(path))

    if log_path:
        f_out = open(log_path, 'a')

    if not os.path.exists(path) and f_out:
        f_out.write('{}: folder is not exists'.format(path))

    now = datetime.now()
    files = get_files(path)
    for f in files:
        try:
            delete = True
            if pattern:
                if not re.search(pattern, os.path.basename(f)): delete = False
            if delete and (now - datetime.fromtimestamp(os.path.getmtime(f))).total_seconds() > seconds:
                os.remove(f)
                if f_out:
                    f_out.write('{}: file is removed ({})\n'.format(f, now.strftime('%d.%m.%Y %H:%M:%S')))
        except OSError as e:
            if f_out: f_out.write(e.strerror)

    if log_path: f_out.close()


def get_files(path):
    file_list = []
    for root, subfolders, files in os.walk(path):
        file_list += [os.path.join(root, f) for f in files]
    return file_list


def get_size(start_path='.'):
    if isfile(start_path):
        return os.path.getsize(start_path)

    total_size = 0
    for dir_path, dir_names, file_names in os.walk(start_path):
        for f in file_names:
            fp = os.path.join(dir_path, f)
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size


def apply_func_to_file_in_folder(path, is_recursion, ignore_patterns, verbose, func, *args):
    files = os.listdir(path)
    for f in files:
        p = os.path.join(path, f)
        if not (ignore_patterns and any([re.search(pattern, f) for pattern in ignore_patterns])):
            if os.path.isdir(p):
                if is_recursion:
                    apply_func_to_file_in_folder(p, is_recursion, ignore_patterns, verbose, func, *args)
            else:
                if verbose:
                    print('{}: applying the function "{}" ({})'.format(os.path.join(path, f), func.__name__, func.__doc__))
                func(os.path.join(path, f), *args)


def _get_file_url_parts(file_url):
    """
    Возвращает две части пути (список с двумя значениями).
    Первая часть - до названия файла, вторая название файла

    :param file_url: str путь до файла
    :return: list список из двух значений (путь до файла, название файла)
    """
    file_url_parts = file_url.rsplit('/', 1)
    if len(file_url_parts) == 1:
        file_url_parts = file_url.rsplit('\\', 1)
    return file_url_parts


def _get_ext(path, with_dot=False):
    """
    Возвращает расширение файла.

    :param path: путь до файла
    :param with_dot: с точкой?
    :return: расширение файла
    """
    if with_dot:
        return os.path.splitext(path)[1]
    else:
        return os.path.splitext(path)[1][1:]


def _get_filename_without_ext(path):
    return os.path.splitext(os.path.basename(path))[0]


def _get_path_without_ext(path):
    return os.path.splitext(path)[0]


def get_free_space():
    def get_free_drive_space(drive):
        if platform.system() == 'Windows':
            free_bytes = ctypes.c_ulonglong(0)
            ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(drive), None, None, ctypes.pointer(free_bytes))
            return free_bytes.value
        else:
            return os.statvfs('/').f_bavail * os.statvfs('/').f_bsize

    my_temp = tempfile.gettempdir()
    my_drive = my_temp[0:2]
    return get_free_drive_space(my_drive)


def delete_file(path):
    if os.path.exists(path):
        os.remove(path)


def delete_file_if_change(old_file, new_file):
    if old_file.name and new_file != old_file:
        delete_file(old_file.path)
        return True
    return False
