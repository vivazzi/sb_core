import decimal
import json
import os
import random
import socket
import subprocess
from datetime import datetime
from os.path import join, dirname, exists

from urllib.request import urlopen

from admin_tools.dashboard import modules
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import mail_admins
from django.template.loader import render_to_string
from django.utils import formats, translation
from sb_core.constants import ALPHABET_AND_DIGITS, DIGITS, STAT_LOG
from django.utils.translation import gettext as _

from sb_core.settings import DB_DIR
from sb_core.utils.stat import Stat

try:
    from post_office import mail
except ImportError:
    mail = True


def get_random_seq(value=5, only_digit=False):
    result = ''
    seq = DIGITS if only_digit else ALPHABET_AND_DIGITS
    for i in range(int(value)):
        result += random.choice(seq)
    return result


def to_log(string, new_line=True):
    path = join(dirname(settings.BASE_DIR), 'logs')
    if not exists(path):
        os.mkdir(path)

    path = join(path, 'project.log')

    with open(path, 'a') as f:
        new_line = '\n' if new_line and os.stat(path).st_size > 0 else ''
        date = datetime.now().strftime('%d.%m.%y %H:%M:%S') if new_line else ''
        f.write('{}{} | {}'.format(new_line, date, string))


def convert_seconds(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    if s and not (m or h):
        return '{} {}.'.format(round(s, 4), _('sec'))
    elif s and m and not h:
        return '{} {}. {} {}.'.format(int(m), _('m'), round(s, 1), _('sec'))
    else:
        return '{} {}. {} {}. {} {}.'.format(int(h), _('h'), int(m), _('m'), round(s), _('sec'))


def custom_convert_seconds(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    if s and not (m or h):
        return '{} {}.'.format(int(s), _('sec'))
    elif s and m and not h:
        return '{} {}. {} {}.'.format(int(m), _('m'), int(s), _('sec'))
    else:
        return '{} {}. {} {}.'.format(int(h), _('h'), int(m), _('m'))


def datetime_format(d):
    return formats.date_format(d, formats.get_format('DATETIME_FORMAT', lang=translation.get_language()))


def date_format(d):
    return formats.date_format(d, formats.get_format('DATE_FORMAT', lang=translation.get_language()))


def time_format(d):
    return formats.date_format(d, formats.get_format('TIME_FORMAT', lang=translation.get_language()))


def transliterate(s, is_slug=False, valid_characters=None, use_dot=False):
    if not isinstance(s, str):
        return None

    if not valid_characters:
        valid_characters = '{}- '.format(ALPHABET_AND_DIGITS)
        if not is_slug:
            valid_characters = '{}\\/._ '.format(valid_characters)

        if use_dot and '.' not in valid_characters:
            valid_characters += '.'

    def check_string(checked_string):
        for char_item in checked_string:
            if char_item not in valid_characters:
                return False

        return True

    capital_letters = {'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'E', 'Ж': 'Zh', 'З': 'Z', 'И': 'I', 'Й': 'Y',
                       'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F',
                       'Х': 'H', 'Ц': 'Ts', 'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sch', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
                       'Я': 'Ya', ' ': '-' if is_slug else '_'}

    lower_case_letters = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'y',
                          'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f',
                          'х': 'h', 'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'sch', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya'}

    translit_string = ''

    for index, char in enumerate(s):
        if char in lower_case_letters:
            char = lower_case_letters[char]
        elif char in capital_letters:
            char = capital_letters[char]
            if len(s) > index + 1:
                if s[index + 1] not in lower_case_letters:
                    char = char.upper()
            else:
                char = char.upper()
        if check_string(char):
            if is_slug: char = char.lower()
            translit_string += char

    return translit_string


def open_file(path, mode):
    if mode == 'a_or_w':
        if exists(path): return open(path, 'a')
        else: return open(path, 'w')

    return open(path, mode)


def apply_filters(objects, params, filter_dict):
    """

    Args:
        objects: ex, Product.objects.all()
        params: ex, request.GET.copy()
        filter_dict: ex, {'cat': 'category', 'status': 'status_title'}

    Returns: filtered objects

    """
    for key, value in filter_dict.items():
        if key not in params:
            del filter_dict[key]

    for key, value in params.items():
        if filter_dict.get(key) and value:
            field_query = {filter_dict[key]: value}
            objects = objects.filter(**field_query)
    return objects


class Managers:
    def __init__(self, group_title):
        group, created = Group.objects.get_or_create(name=group_title)

        self.managers = group.user_set.all()
        if created or not self.managers:
            user = get_user_model().objects.first()
            user.groups.add(group)

            site_name = get_current_site(None).name
            data = {'site': site_name, 'group': group_title, 'user': user}
            if created:
                template = 'sb_core/email/group_created.html'
                subject = '[{0}] Создана группа пользователей "{1}"'
            else:
                template = 'sb_core/email/add_users_to_group.html'
                subject = '[{0}] Добавьте сотрудников в группу "{1}"'
            msg = render_to_string(template, data)
            mail_admins(subject.format(site_name, group_title), msg)

            self.managers = (user, )

    def get_managers(self):
        return self.managers

    def get_manager_emails(self):
        return [manager.email for manager in self.managers]

    def send_email(self, subject, message, me=None, headers=None):
        if not mail:
            raise Exception('Method send_email() requires post_office')

        for manager in self.managers:
            translation.activate(manager.djangocms_usersettings.language)

            if not me:
                me = '{} <{}>'.format(settings.SHORT_COMPANY_NAME, settings.EMAIL_HOST_USER)

            if not headers:
                headers = {'To': '{} {} - {}  <{}>'.format(_('Manager'), manager.get_full_name(),
                                                           settings.SHORT_COMPANY_NAME, manager.email)}
            mail.send(manager.email, me, subject=subject, message=message, html_message=message, headers=headers)
            translation.deactivate()

    @staticmethod
    def get_manager_name(manager, company_name_genitive=None):
        if manager.get_full_name():
            return 'Менеджер {} - {} <{}>'.format(company_name_genitive, manager.get_full_name(), manager.email)
        else:
            return 'Менеджер {} <{}>'.format(company_name_genitive, manager.email)


def has_user_in_group(user, group, allow_for_superuser=False):
    return allow_for_superuser and user.is_superuser or group in user.groups.values_list('name', flat=True)


def is_true(value):
    return value is True or value == 'true' or value == 'True'


def convert_to_point(value):
    return str(value).replace(',', '.')


def get_phone(v):
    p = ''
    for w in v:
        if w in DIGITS: p += w

    l = len(p)
    if l == 5:
        return '{}-{}-{}'.format(p[0], p[1:3], p[3:])
    if l == 6:
        return '{}-{}-{}'.format(p[0:2], p[2:4], p[4:])
    if l == 10:
        return '{} {} {} {}'.format(p[0:3], p[3:5], p[5:7], p[7:])
    if l == 11:
        return '{} {} {} {} {}'.format(p[0], p[1:4], p[4:6], p[6:8], p[8:])

    return p


class DecoratorChainingMixin(object):
    def dispatch(self, *args, **kwargs):
        decorators = getattr(self, 'decorators', [])
        base = super(DecoratorChainingMixin, self).dispatch

        for decorator in decorators:
            base = decorator(base)
        return base(*args, **kwargs)


class MessagePool:
    messages = []

    def __init__(self):
        def check_pages(urls):
            if settings.DOMAIN == 'http://localhost:8000/' or settings.DOMAIN == 'localhost:8000':
                self.messages.append({'status': 'neutral',
                                      'message': 'На {} доступность страниц не проверяется.'.format(settings.DOMAIN)})
                for url in urls:
                    if not exists(join(settings.BASE_DIR, url)):
                        self.messages.append({'status': 'error',
                                              'message': '{} не существует'.format(url)})
            else:
                for url in urls:
                    try:
                        code = urlopen(join(settings.DOMAIN, url)).getcode()
                        if code not in [200, 301]:
                            self.messages.append({'status': 'error',
                                                  'message': '{} не доступен'.format(url)})
                    except (socket.error, IOError) as e:
                        self.messages.append({'status': 'error',
                                              'message': '{} не доступен'.format(url)})

        urls = ['sitemap.xml', 'robots.txt', ]
        # check_pages(urls)

    def add(self, mes):
        """
         Ex: message_pool.add({'status': 'error', 'message': 'test'})
        """
        self.messages.append(mes)


message_pool = MessagePool()


class MessageModule(modules.DashboardModule):
    def is_empty(self):
        return False

    def __init__(self, **kwargs):
        super(MessageModule, self).__init__(**kwargs)
        self.template = 'admin/dashboard/message_module.html'

    def init_with_context(self, context):
        if self._initialized:
            return

        self._initialized = True
        context['messages'] = message_pool.messages


def humanize_bytes(n, precision=2):
    # Author: Doug Latornell
    # Licence: MIT
    # URL: http://code.activestate.com/recipes/577081/
    """Return a humanized string representation of a number of bytes.

    >>> humanize_bytes(1)
    '1 B'
    >>> humanize_bytes(1024, precision=1)
    '1.0 kB'
    >>> humanize_bytes(1024 * 123, precision=1)
    '123.0 kB'
    >>> humanize_bytes(1024 * 12342, precision=1)
    '12.1 MB'
    >>> humanize_bytes(1024 * 12342, precision=2)
    '12.05 MB'
    >>> humanize_bytes(1024 * 1234, precision=2)
    '1.21 MB'
    >>> humanize_bytes(1024 * 1234 * 1111, precision=2)
    '1.31 GB'
    >>> humanize_bytes(1024 * 1234 * 1111, precision=1)
    '1.3 GB'

    """
    abbrevs = [
        (1 << 50, 'PB'),
        (1 << 40, 'TB'),
        (1 << 30, 'GB'),
        (1 << 20, 'MB'),
        (1 << 10, 'kB'),
        (1, 'B')
    ]

    if n == 1:
        return '1 b'

    for factor, suffix in abbrevs:
        if n >= factor:
            break

    # noinspection PyUnboundLocalVariable
    return '%.*f %s' % (precision, float(n) / factor, suffix)


def bool_js_to_py(v):
    if v == 'true': return True
    if v == 'false': return False
    return v


def bool_py_to_js(v):
    if v is True: return 'true'
    if v is False: return 'false'
    return v


def d_print(string, end='\n', force=False):
    if settings.DEBUG or force:
        print(string, end=end)


def get_index_from_str(source, v, exclude_v=False):
    if v:
        repeats = 1
        if type(v) in (tuple, list):
            repeats = v[1]
            v = v[0]

        index = -1
        for repeat in range(repeats):
            index = source.find(v, index + 1)
            if index == -1:
                raise Exception('"{}" is not found in {}'.format(v, source))

        if exclude_v:
            index += len(v)

        return index

    return None


def slice_str(source, str_1=None, str_2=None, exclude_str_1=False, exclude_str_2=False):
    return source[get_index_from_str(source, str_1, exclude_str_1):get_index_from_str(source, str_2, exclude_str_2)]


def set_perms_to_media():
    if not settings.DEBUG:
        p = subprocess.Popen('chown -R www-data:www-data {}'.format(join(settings.BASE_DIR, 'media')), shell=True)
        p.wait()


def send_mails_if_debug():
    if settings.DEBUG and settings.EMAIL_BACKEND == 'post_office.EmailBackend':
        from django.core.management import call_command
        call_command('send_queued_mail')


def with_end(number, array, show_number=True):
    """
        Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
        number int - Число на основе которого нужно сформировать окончание
        Array Массив слов, разделяющиеся между собой пробелами, например 'яблоко_яблока_яблок'
        (1 яблоко, 2 яблока, много яблок -> 'яблоко_яблока_яблок')
    """
    array = array.split('_')
    result = array[2]
    i = number % 100
    if 11 <= number <= 19:
        result = array[2]
    else:
        i %= 10
        if i == 1:
            result = array[0]
        if 1 < i <= 4:
            result = array[1]
        if i > 4:
            result = array[2]

    if show_number:
        return '{} {}'.format(number, result)

    return result


def separate_thousand(value, char=' '):
    result = ''
    parts = str(value).split()
    for part in parts:
        try:
            n = '{0:,} '.format(decimal.Decimal(part))
            if char != ',':
                n = n.replace(',', char)

            result += n
        except decimal.InvalidOperation:
            result += '{} '.format(part)
    return result.replace('  ', ' ').strip()


def str_to_args_for_management_commands(string):
    row_params = string.split()
    params = []
    i = -1
    for par in row_params:
        if par.startswith('-'):
            params.append(par)
            i += 1
        else:
            params[i] += '={}'.format(par)

    return params


def print_timedelta(delay):
    return str(delay).rsplit('.')[0].replace('days', _('days')).replace('day', _('day'))


def get_stat():
    stat = Stat(DB_DIR)

    if exists(stat.path):
        with open(stat.path) as f:
            stat.data = json.loads(f.read().strip())

    return stat
