import os
import shutil

from os.path import exists, join, dirname, basename

from PIL import Image
from django.apps import apps
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.html import escape

from sb_core.constants import MAX_THUMB_WIDTH, MAX_THUMB_HEIGHT
from sb_core.exceptions import ImageException, ThumbnailException
from sb_core.folder_mechanism import THUMBNAIL_PART, upload_to_handler
from sb_core.settings import PIC_FIELD_OPTIMIZE_PARS
from sb_core.signals import thumbnail_created
from sb_core.utils.image import get_no_image_url
from sb_core.utils.sb_utils import transliterate, get_random_seq
from sb_core.utils.os_utils import _get_path_without_ext, _get_ext, _get_filename_without_ext, _get_file_url_parts


def crop_image(pic_path, width, height, type_of_min='contain', new_pic_path=None, postfix=None):
    def correct_size(pic_w, pic_h, th_w, th_h):
        """
            adjusts thumbnail size, depending on picture size
        """
        aspect_ratio = float(pic_w) / pic_h

        min_side = 'width' if aspect_ratio < 1 else 'height'

        if min_side == 'width':
            return th_w, th_w / aspect_ratio
        else:
            if th_h * aspect_ratio >= th_w:
                return th_h * aspect_ratio, th_h
            else:
                return th_w, th_w / aspect_ratio

    if not any([width, height]):
        raise ImageException('{} | Resize error: specify new object sizes'.format(pic_path))

    old_path = pic_path
    img = Image.open(pic_path)
    old_size = img.size
    if type_of_min == 'contain':
        img.thumbnail((width, height), Image.Resampling.LANCZOS)
    if type_of_min == 'cover':
        # does not increase thumbnail if the width or height of image is smaller than width or height of a thumbnail
        # this function is in thumbnail method
        img.thumbnail(correct_size(old_size[0], old_size[1], width, height), Image.Resampling.LANCZOS)

    if new_pic_path: pic_path = new_pic_path

    ext = _get_ext(pic_path, True)
    if postfix:
        pic_path = '{}{}{}'.format(_get_path_without_ext(pic_path), postfix, ext)

    if (old_size != img.size or (width >= img.size[0] and height >= img.size[1]) or not exists(pic_path)) and ext:
        try:
            attrs = {
                'optimize': True
            }
            if img.mode in ('RGB', 'L', 'CMYK'):
                attrs.update({'format': 'JPEG', 'quality': PIC_FIELD_OPTIMIZE_PARS['quality']})
            else:
                attrs.update({'format': 'PNG', 'quality': 100})  # 100 is the longest by time, but the most compressed

            img.save(pic_path, **attrs)
            return

        except KeyError:
            pass

    # just copy original without modifications in other cases
    shutil.copyfile(old_path, pic_path)


def crop_image_by_model_field(pic, width, height, type_of_min='contain', new_image_path=None, postfix=None):
    crop_image(pic.path, width, height, type_of_min, new_image_path, postfix)


def get_thumb_name(pic_path, filename, postfix):
    if postfix:
        filename = '{}{}{}'.format(_get_filename_without_ext(pic_path), postfix, _get_ext(pic_path, True))

    return '{}{}'.format(THUMBNAIL_PART, filename)


def get_thumbnail_url(pic_url, postfix=None):
    pic_url_parts = pic_url.rsplit('/', 1)
    return join(pic_url_parts[0], get_thumb_name(pic_url, pic_url_parts[1], postfix))


def get_thumbnail_path(pic_path, postfix=None):
    dir_name, filename = os.path.split(pic_path)
    return join(dir_name, get_thumb_name(pic_path, filename, postfix))


def create_thumbnail(pic, width=None, height=None, type_of_min='contain', postfix=None):
    if not any([width, height]):
        raise ThumbnailException('You need specify width and/or height of thumbnail')

    pic_path = pic.path
    thumb_path = get_thumbnail_path(pic_path)
    delete_thumbnail(pic_path, postfix)

    if not width: width = height
    if not height: height = width
    crop_image_by_model_field(pic, width, height, type_of_min, thumb_path, postfix)

    thumbnail_created.send(sender=pic.field.model, pic=pic, path=get_thumbnail_path(pic.path, postfix))


def create_ghost_thumbnail(pic, postfix=None):
    pic_path = pic.path
    new_pic_path = join(_get_file_url_parts(pic_path)[0], get_thumb_name(pic_path, os.path.basename(pic_path), postfix))
    shutil.copy(pic_path, new_pic_path)


def delete_thumbnail(pic_path, postfix=None):
    thumb_path = get_thumbnail_path(pic_path, postfix)
    if exists(thumb_path): os.remove(thumb_path)


def thumb(pic, width=0, height=0, th_type='cover', postfix=None):
    if bool(pic):
        thumb_path = get_thumbnail_path(pic.path, postfix)
        if not exists(thumb_path):
            if exists(pic.path):

                if width == 0 and height == 0:
                    aspect_ratio = float(pic.width) / pic.height
                    if aspect_ratio >= 1: width = MAX_THUMB_WIDTH
                    else: height = MAX_THUMB_HEIGHT

                create_thumbnail(pic, width, height, th_type, postfix)
            else:
                return 'bad_url:{}'.format(pic.url)
        return get_thumbnail_url(pic.url, postfix)

    return ''


def generate_pic_tag(tag, pic, w, h, x, postfix, th_type='cover', fluid=True, from_ajax=False, **kwargs):
    def get_v(items):
        res = {}
        for i in items:
            if i:
                i_k, i_v = [kv.strip() for kv in i.split(':')]
                res.update({i_k: i_v})

        return res

    has_content_th = kwargs.pop('has_content_th', '')

    attrs = []

    if not pic.width:
        return ''

    aspect_ratio_pic = float(pic.width) / pic.height
    if w == 0 or h == 0:
        if w == 0 and h == 0:
            if aspect_ratio_pic >= 1: w = MAX_THUMB_WIDTH
            else: h = MAX_THUMB_HEIGHT
        else:
            if w == 0: w = h * aspect_ratio_pic
            if h == 0: h = w / aspect_ratio_pic

    styles = {}
    if tag == 'span':
        styles.update({'display': 'inline-block'})

    if tag == 'img':
        aspect_ratio_wr = float(w) / h
        if th_type == 'cover':
            if aspect_ratio_wr > aspect_ratio_pic: styles.update({'width': '100%'})
            else: styles.update({'height': '100%'})
        else:
            if aspect_ratio_wr > aspect_ratio_pic: styles.update({'height': '100%'})
            else: styles.update({'width': '100%'})
        styles.update({'position': 'absolute', 'top': '0', 'bottom': '0', 'left': '0', 'right': '0', 'margin': 'auto'})
    else:
        if fluid: styles.update({'padding-top': '{}%'.format((float(h)/w) * 100)})
        else: styles.update({'width': '{}px'.format(w), 'height': '{}px'.format(h)})

    override_styles = kwargs.pop('style', None)
    if override_styles:
        override_styles = get_v([item.strip() for item in override_styles.split(';')])
        styles.update(override_styles)
    style_items = ';'.join(['{}: {}'.format(k1, v1) for k1, v1 in styles.items()])

    postfixes = [postfix, '{0}_x2'.format(postfix)]
    postfix = postfixes[x - 1]
    w *= x
    h *= x

    if tag == 'img':
        attrs.append('src="{}"'.format(thumb(pic, w, h, th_type, postfix)))
    else:
        bg_css = kwargs.pop('bg_css', None)

        if not bg_css:
            if from_ajax: bg_css = "background-image: url('{}')"
            else: bg_css = r'background-image: url(\'{}\')'

        bg = bg_css.format(thumb(pic, w, h, th_type, postfix))

        style_items += ';{}'.format(bg)

    attrs.append('style="{};"'.format(style_items))

    for k, v in kwargs.items():
        if type(v) != dict and v:
            attrs.append('{}="{}"'.format(k, v))

    attrs = ' '.join(attrs)
    if has_content_th:
        return mark_safe('<{} {}>'.format(tag, attrs))

    if tag == 'img':
        return mark_safe('<{} {} />'.format(tag, attrs))

    return mark_safe('<{0} {1}></{0}>'.format(tag, attrs))


def retina(obj, w=0, h=0, postfix=None, title='', th_type='cover', fluid=False, tag='div', is_retina=None, from_ajax=False, pic_field='pic', **kwargs):
    def get_wr_tag(_tag, _kwargs):
        if kwargs.get('wr_tag'): return kwargs.pop('wr_tag')
        if tag == 'img': return 'div'
        return tag

    if postfix is None:
        postfix = '_{}x{}'.format(w, h)

    context = {'wr_tag': get_wr_tag(tag, kwargs),
               'wr_class': kwargs.pop('wr_class', 'th_wr'),
               'wr_style': kwargs.pop('wr_style', ''),
               'class': 'th_0 th_{} {}'.format(th_type, kwargs.pop('th_class', 'th')),
               'style': '',
               'silent': kwargs.pop('silent', False),
               'no_check': kwargs.pop('no_check', False),
               'title': escape(title),
               'content_wr': kwargs.pop('content_wr', ''),
               'content_th': kwargs.pop('content_th', ''),
               'use_wr': kwargs.pop('use_wr', True)}

    pic_field = kwargs.pop('pic', '') or pic_field  # ugly hack, workaround to avoid bug
    pic = obj.get_file(pic_field)
    if context['no_check'] or obj and obj.file_exists(pic_field):
        has_content_th = True if context['content_th'] else False

        pic_tag_context = {'title': context['title'],
                           'has_content_th': has_content_th,
                           'class': context['class']}

        if tag == 'img':
            pic_tag_context['alt'] = pic_tag_context['title']

        pic_tag_context.update(kwargs)

        if tag == 'img':
            wr_styles = {'overflow': 'hidden'}

            aspect_ratio_pic = float(pic.width) / pic.height
            if w == 0 or h == 0:
                if w == 0 and h == 0:
                    if aspect_ratio_pic >= 1: w = MAX_THUMB_WIDTH
                    else: h = MAX_THUMB_HEIGHT
                else:
                    if w == 0: w = h * aspect_ratio_pic
                    if h == 0: h = w / aspect_ratio_pic

            if fluid:
                wr_styles.update({'padding-top': '{}%'.format((float(h)/w) * 100)})
                wr_styles.update({'max-width': '{}px'.format(w),
                                  'max-height': '{}px'.format(h),
                                  'width': '100%'})
            else:
                wr_styles.update({'width': '{}px'.format(w), 'height': '{}px'.format(h)})

            wr_styles.update({'position': 'relative'})

            wr_style_items = ';'.join(['{}: {}'.format(k1, v1) for k1, v1 in wr_styles.items()])

            context['wr_style'] = '{};{}'.format(wr_style_items, context['wr_style'])

            if th_type == 'cover':
                aspect_ratio_wr = float(w) / h
                if aspect_ratio_wr < aspect_ratio_pic:
                    pic_tag_context['class'] = '{} left_fix'.format(pic_tag_context['class'])

        if context['content_th']:
            context['html_tag_end'] = mark_safe('</{}>'.format(tag))

        if is_retina is False:
            context['html_tag'] = generate_pic_tag(tag, pic, w, h, 1, postfix, th_type, fluid, from_ajax, **pic_tag_context)
            return mark_safe(render_to_string('retina/_one_variant.html', context))

        if is_retina is True:
            context['html_tag'] = generate_pic_tag(tag, pic, w, h, 2, postfix, th_type, fluid, from_ajax, **pic_tag_context)
            return mark_safe(render_to_string('retina/_one_variant.html', context))

        context['html_tag'] = generate_pic_tag(tag, pic, w, h, 1, postfix, th_type, fluid, from_ajax, **pic_tag_context)
        context['html_tag_x2'] = generate_pic_tag(tag, pic, w, h, 2, postfix, th_type, fluid, from_ajax, **pic_tag_context)

        t = 'retina_to_th.html' if context['content_th'] else 'retina.html'
        context['html_tag_noscript'] = mark_safe(context['html_tag'].replace("\\'", "'"))
        return mark_safe(render_to_string('retina/{}'.format(t), context))

    if context['silent']:
        return ''

    context.update({'w': w,
                    'h': h,
                    'th_type': th_type,
                    'fluid': fluid,
                    'tag': tag,
                    'bad_url': getattr(pic, 'url', None) if pic else 'undefined',
                    'no_image_url': get_no_image_url(w, h)})

    return mark_safe(render_to_string('retina/_no_image.html', context))


def get_thumbnail_html(obj, **kwargs):
    context = {'w': obj.thumb_size[0], 'h': obj.thumb_size[1]}
    context.update(kwargs)

    if context['pic'] == '':
        context['pic'] = 'pic'

    if obj.file_exists(context['pic']) and getattr(obj, context['pic']).is_svg():
        return svg(obj, context['w'], context['h'], pic_field=context['pic'])
    #     html = '<span class="th_wr" style="width: {}px;height: {}px;">
    # <div class="th th_svg" style="background: url(\'{}\') no-repeat 50%;height: 100%;background-size: contain;"></div>
    # </span>'
    #     return html.format(context['w'], context['h'], getattr(obj, context['pic']).url, obj.get_padding_top())

    return retina(obj, **context)


def get_pic_name(pic_obj, pic):
    orig_name = transliterate(pic.name)
    name = transliterate(pic.name)
    while exists(get_thumbnail_path(join(settings.MEDIA_ROOT, upload_to_handler(pic_obj, name)), '')):
        name = '{}_{}'.format(orig_name, get_random_seq(5))
    return name


def delete_thumbs(pic):
    if bool(pic) and exists(pic.path):
        folder = dirname(pic.path)
        [os.remove(join(folder, f)) for f in os.listdir(folder) if f != basename(pic.name)]


def remove_all_thumbs_from_models():
    from sb_core.models import PicModel

    for model in apps.get_models():
        if issubclass(model, PicModel):
            offset = 0
            delta_offset = 100
            objects = model.objects.all()
            objects_count = objects.count()
            while offset < objects_count:
                print(f'INFO: {model._meta.app_label}.{model._meta.object_name}, offset = {offset}')
                for obj in objects[offset:offset+delta_offset]:
                    obj.remove_all_thumbs()

                offset += delta_offset

    print('\nDone!')


def optimize_pic(file_path):
    with Image.open(file_path) as new_pic:
        new_pic.thumbnail(
            (PIC_FIELD_OPTIMIZE_PARS['max_size']['width'], PIC_FIELD_OPTIMIZE_PARS['max_size']['height']),
            resample=Image.Resampling.LANCZOS,
        )

        attrs = {
            'optimize': True
        }
        if new_pic.mode in ('RGB', 'L', 'CMYK'):
            attrs.update({'format': 'JPEG', 'quality': PIC_FIELD_OPTIMIZE_PARS['quality']})
        else:
            attrs.update({'format': 'PNG', 'quality': 100})  # 100 is the longest by time, but the most compressed

        new_pic.save(file_path, **attrs)
        new_pic.close()


def svg(obj, w, h=None, hint='', style='', pic_field='pic'):
    pic = getattr(obj, pic_field)
    padding_top = '{}%'.format(round(pic.height / pic.width, 6) * 100) if exists(pic.path) and pic.width and pic.height else 0
    return render_to_string('retina/svg.html', {'obj': obj, 'pic': pic, 'width': w, 'height': h, 'padding_top': padding_top,
                                                'hint': hint, 'style': style})
