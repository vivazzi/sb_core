from os.path import join, dirname, exists
from django.conf import settings


def check_pages(urls):
    if settings.DOMAIN == 'http://localhost:8000/' or settings.DOMAIN == 'localhost:8000':
        self.messages.append({'status': 'neutral',
                              'message': 'На {} доступность страниц не проверяется.'.format(settings.DOMAIN)})
        for url in urls:
            if not exists(join(settings.BASE_DIR, url)):
                self.messages.append({'status': 'error',
                                      'message': '{} не существует'.format(url)})
    else:
        for url in urls:
            try:
                code = urllib.urlopen(join(settings.DOMAIN, url)).getcode()
                if code not in [200, 301]:
                    self.messages.append({'status': 'error',
                                          'message': '{} не доступен'.format(url)})
            except (socket.error, IOError) as e:
                self.messages.append({'status': 'error',
                                      'message': '{} не доступен'.format(url)})

urls = ['sitemap.xml', 'robots.txt', ]
# check_pages(urls)
