import json

from sb_core.constants import STAT_LOG


class Stat(object):
    data = {
        'sizes': {
            'project_folder': {
                'size': 0, 'size_str': '',
            },
            'containers': {
                'list': {
                    # '<project>_compose_prod': {
                    #     'name': '<container_name>',
                    #     'size': 0, 'size_str': '',
                    #     'description': '<description>',
                    # },
                },
                'total': 0, 'total_str': '',
            },
            'volumes': {
                'list': {
                    # '<name>': {
                    #     'path': <path>,
                    #     'description': '<description>',
                    #     'size': 0, 'size_str': '',
                    # },
                },
                'total': 0, 'total_str': '',
            },
            'backups': {
                'total': 0, 'total_str': '',
                'files': {
                    # some files. Real date instead of %d_%m_%Y__%H_%M_%S
                    # 'db_project_%d_%m_%Y__%H_%M_%S.tar.gz': {'size': 0, 'size_str': ''},
                    # 'media_project_%d_%m_%Y__%H_%M_%S.tar.gz': {'size': 0, 'size_str': ''},
                    # 'code_project_%d_%m_%Y__%H_%M_%S.tar.gz': {'size': 0, 'size_str': ''}
                }
            },
            'total': 0, 'total_str': '',
        },
        'backup_date': 0, 'backup_date_str': '',
        'elapsed_time': 0, 'elapsed_time_str': '',
    }

    def __init__(self, backup_project_dir):
        self.path = backup_project_dir / STAT_LOG

    def save(self):
        with open(self.path, 'w', encoding='utf-8') as f:
            f.write(f'{json.dumps(self.data, indent=4, sort_keys=True)}\n')
