import os
from os.path import exists, join, basename, splitext
from django.conf import settings
from django.utils.encoding import iri_to_uri

from sb_core.utils.sb_utils import transliterate
from sb_core.utils.url_utils import get_saved_resource, get_resource
from sb_core.folder_mechanism import generate_folder


def save_dummy_pic(title, pic_model, color='#addea5', w=800, h=600, save=False, path='dummy_pics'):
    pic = get_dummy_pic(title, color, w, h, save, path)
    return save_pic(pic, title, pic_model)


def save_url_pic(url, pic_model, title=''):
    pic = get_resource(url, encoding=None)
    return save_pic(pic, title or basename(url), pic_model)


def save_path_pic(path, pic_model, title=''):
    """
    For example:

    pic_path, folder = save_path_pic(join(settings.STATIC_ROOT, 'template/images/logo_for_pic.png'), MyModel)
    obj.folder = folder
    obj.pic = pic_path
    obj.save(update_fields=('folder', 'pic'))

    :param path:
    :param pic_model:
    :param title:
    :return:
    """
    with open(path, 'rb') as f:
        pic = f.read()
        return save_pic(pic, title or basename(path), pic_model)


def save_pic(img, title, pic_model):
    folder = generate_folder(join(settings.MEDIA_ROOT, pic_model.upload_to), 5)

    img_dir = join(pic_model.upload_to, folder)
    img_name = transliterate(title, True, use_dot=True).replace('%', '').replace('?', '').replace('=', '')
    ext = splitext(basename(img_name))[1][1:]
    if ext not in ('bmp', 'jpg', 'jpeg', 'png', 'ico'):
        img_name = '{}.png'.format(img_name)
    img_path = join(img_dir, img_name)

    path = join(settings.MEDIA_ROOT, img_path)

    if not exists(join(settings.MEDIA_ROOT, img_dir)):
        os.makedirs(join(settings.MEDIA_ROOT, img_dir))

    with open(path, 'wb') as f:
        f.write(img)

    return img_path, folder


def get_dummy_pic(title, color='#addea5', w=800, h=600, save=False, path='dummy_pics', use_size_as_title=False):
    if not w and not h: w = h = 100
    elif not w: w = h
    elif not h: h = w

    size = 'x'.join([str(w), str(h)])
    color = color.replace('#', '')

    font_size = ''
    if w >= 200:
        sep = '+'
    else:
        sep = '|'
        font_size = 's=13&'

    uri_title = iri_to_uri(title.replace(' ', sep))
    url = ('http://ipsumimage.appspot.com/{},{}?{}l={}'.format(size, color, font_size, uri_title))

    if save:
        title = '{}.png'.format(size if use_size_as_title else transliterate(title, True))
        resource, url = get_saved_resource(url, path, title, encoding=None, tryings=1, silent=True)
        return resource, url

    return get_resource(url, encoding=None)


def get_no_image_url(w=None, h=None):
    return get_dummy_pic('Нет изображения', '#f6f6f6', w, h, True, 'no_images', False)[1]
