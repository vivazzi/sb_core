import os
import time
import random
import requests

from django.conf import settings
from os.path import join, basename, dirname, exists

from django.core.cache import cache
from django.contrib.sites.shortcuts import get_current_site

from sb_core.constants import ALPHABET_AND_DIGITS
from sb_core.utils.sb_utils import d_print, transliterate


def _get_cache_path(cache_path):
    filename = basename(cache_path)
    inserts = len(filename) // 255
    if inserts:

        for i in range(inserts):
            char_index = (i + 1) * 255 - i
            filename = '{}/{}'.format(filename[0:char_index], filename[char_index:])

        cache_dir = '{}/{}'.format(dirname(cache_path), dirname(filename))
        cache_path = '{}/{}'.format(cache_dir, basename(filename))
    return cache_path


def _save_resource(resource, cache_path, url, save_url_in_file=False, encoding='utf-8'):
    cache_dir = dirname(cache_path)

    if not exists(cache_dir):
        os.makedirs(cache_dir)

    f_type = 'w'
    if not encoding:
        f_type = 'wb'

    with open(cache_path, f_type, encoding=encoding) as f:
        if save_url_in_file:
            resource = '<!-- {} --> \n {}'.format(url, resource)

        f.write(resource)

    d_print('Saved resource {} in {}'.format(url, cache_dir))


def _prepare_url(url):
    return url.replace('/', '_').replace(':', '_')


def _prepare_data(url, folder, filename):
    valid_characters = '{}-=_.'.format(ALPHABET_AND_DIGITS)
    filename = filename or transliterate(_prepare_url(url), True, valid_characters)
    media_path = join(settings.MEDIA_URL, folder, filename)
    return url, media_path, filename


def _get_headers(site):
    user_agents = [
        'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.3) Gecko/20090913 Firefox/3.5.3',
        'Mozilla/5.0 (Windows; U; Windows NT 6.1; en; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)',
        'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.1) Gecko/20090718 Firefox/3.5.1',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, like Gecko) Chrome/4.0.219.6 Safari/532.1',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.2)',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.5.30729; .NET CLR 3.0.30729)',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Win64; x64; Trident/4.0)',
        'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; .NET CLR 2.0.50727; InfoPath.2)',
        'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
        'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP)',
        'Opera/9.80 (Windows NT 5.2; U; ru) Presto/2.5.22 Version/10.51',
        # new
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36',
        'Mozilla/5.0 (Linux; Android 4.2.2; ru-ru; SAMSUNG GT-19500 Build/JDQ39) AppleWebKit/535.19 (KHTML, like Gecko) Version/1.0 Chrome/18.0.1025.308 Mobile Safari/535.19'

        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6',
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.6) Gecko/2009011912 Firefox/3.0.6',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.6) Gecko/2009020911 Ubuntu/8.10 (intrepid) Firefox/3.0.6',
        'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6',
        'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)',
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.48 Safari/525.19',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',
        'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.6) Gecko/2009020911 Ubuntu/8.10 (intrepid) Firefox/3.0.6',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.5) Gecko/2008121621 Ubuntu/8.04 (hardy) Firefox/3.0.5',
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-us) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/3.2.1 Safari/525.27.1',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    ]
    refers = ['https://www.google.com/?q=',
              'https://www.google.ru/?q=',
              'http://yandex.ru/yandsearch?text=',
              'http://go.mail.ru/search?&q=',
              'http://nova.rambler.ru/search?query=']

    return {'User-Agent': random.choice(user_agents),
            'Cache-Control': random.choice(['no-cache', 'cache']),
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'Referer': '%s%s' % (random.choice(refers), site),
            'Keep-Alive': random.randint(110, 120),
            'Connection': 'keep-alive',
            'cookie': '_ym_uid=1490981946218677915; longLoadingParts=0; _ym_isad=2; Cars=Modifications=166ede70-b24e-47e1-bf63-e420fb733d07,2015,0|e310d371-b082-4070-8882-81ebf4ba582b,2014,0|446a7544-321b-417c-a7bc-73260720483d,2003,0||54d5707e-3594-4017-9029-b7788ecda359,2016,1&SearchByCar=True; ASP.NET_SessionId=lm2fzbmde1wkkzvwmwr5e0rh; _gat=1; _ga=GA1.2.38282003.1490981946; _gid=GA1.2.1724980795.1495144779; _dc_gtm_UA-8605983-52=1'}


def get_proxies():
    cache_key = 'sb_get_proxies'
    res = cache.get(cache_key)
    if res:
        return res

    html = get_resource('http://www.ip-adress.com/proxy_list/')
    try:
        from bs4 import BeautifulSoup
        soup = BeautifulSoup(html, 'html5lib')
    except ImportError:
        raise Exception('You need install beautifulsoup4 package if you want use use_proxy parameter in get_resource()')

    trs = soup.find(class_='proxylist').find_all('tr')
    res = ['http://{}'.format(tr.td.text) for tr in trs[1:]]

    cache.set(cache_key, res, 180)
    return res


def get_resource(url, sleep_time=0, sleep_time_if_error=60, tryings=1, headers=False, use_proxy=False, encoding='utf-8', silent=False,
                 data=None):
    url = url.replace(' ', '%20')
    trying = 0
    while True:
        kwargs = {}
        if headers: kwargs.update({'headers': _get_headers(get_current_site(None).name) if headers is True else headers})
        if use_proxy: kwargs.update({'proxies': {'http': random.choice(get_proxies())}})

        response = requests.post(url, data=data, **kwargs) if data else requests.get(url, **kwargs)

        if response.status_code == 200:
            break

        trying += 1

        if tryings != 0 and trying == tryings:
            if silent:
                return None

            raise Exception('All tryings {} is fails.'.format(tryings))

        if response.status_code == 503 or response.status_code == 104:
            trying_str = trying if tryings == 0 else '{} from {}'.format(trying, tryings)
            d_print('503 Error {} | trying {} is fail. Sleeping now in {} seconds...'.format(url, trying_str, sleep_time_if_error))
            time.sleep(sleep_time_if_error)

    if sleep_time:
        if type(sleep_time) in (tuple, list):
            sleep_time = random.uniform(sleep_time[0], sleep_time[1])
        time.sleep(sleep_time)

    content = response.content
    if encoding:
        content = content.decode(encoding)

    return content


def get_saved_resource(url, folder='uploaded_resources', filename='',
                       sleep_time=0, sleep_time_if_error=60, tryings=20, save_url_in_file=False,
                       use_headers=False, use_proxy=False, encoding='utf-8', silent=False):
    """
    :return: resource, media_path
    """
    url, media_path, filename = _prepare_data(url, folder, filename)

    cache_dir = join(settings.MEDIA_ROOT, folder)
    cache_path = join(cache_dir, filename)
    cache_path = _get_cache_path(cache_path)
    if exists(cache_path):
        f_type = 'r' if encoding else 'rb'
        with open(cache_path, f_type, encoding=encoding) as f:
            resource = f.read()
            return resource, media_path

    resource = get_resource(url, sleep_time, sleep_time_if_error, tryings, use_headers, use_proxy, encoding, silent)
    if resource:
        _save_resource(resource, cache_path, url, save_url_in_file, encoding)

    return resource, media_path


def get_debug_resource(url, folder='uploaded_resources', filename='', force_load_resource_in_debug=False,
                       sleep_time=0, sleep_time_if_error=60, tryings=20, save_url_in_file=False,
                       use_headers=False, use_proxy=False, encoding='utf-8', silent=False):
    if not force_load_resource_in_debug and settings.DEBUG:
        return get_saved_resource(url, folder, filename, sleep_time, sleep_time_if_error, tryings, save_url_in_file,
                                  use_headers, use_proxy, encoding)

    url, media_path, filename = _prepare_data(url, folder, filename)
    return get_resource(url, sleep_time, sleep_time_if_error, tryings, use_headers, use_proxy, encoding, silent), ''
