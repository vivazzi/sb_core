import sys


class ProgressBar(object):
    def __init__(self, value, block_char='#', use_percent=True):
        self.max_value = value
        self.block_count = 0
        self.block = block_char
        self.use_percent = use_percent
        self.f = sys.stdout
        self.len_progressbar = 50
        self.percent_of_one = 100 / float(self.len_progressbar)
        if not self.max_value: return
        self.update(0)

    def update(self, count):
        count = min(count, self.max_value)
        percent = round(100.0 * count / self.max_value, 2) if self.max_value else 100

        if not self.use_percent:
            block_count = int(percent // 2)
            if block_count <= self.block_count: return
            self.block_count = block_count

        self.f.write('\r|')
        for i in range(self.len_progressbar):
            char = self.block if i * self.percent_of_one < percent else '_'
            self.f.write(char)

        self.f.write('| Completed {}%'.format(percent) if self.use_percent else '|')
        self.f.flush()

        if percent == 100:
            self.f.write('\n')
