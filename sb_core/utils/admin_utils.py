
def seo_fieldset(is_collapse=True):
    classes = []
    if is_collapse:
        classes.append('collapse')

    return ('SEO', {
        'classes': classes,
        'fields': ('meta_tags', 'meta_desc')
    })
