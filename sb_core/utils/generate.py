from django.core.exceptions import ImproperlyConfigured

from sb_core.utils.sb_utils import get_random_seq


def get_unique_slug(model, checked_slug, excluded_obj=None, par='slug'):
    """
    Examples:

    def save(self, *args, **kwargs):
        if self.slug:
            self.slug = get_unique_slug(self._meta.model, self.slug, self)

        return super(Product, self).save(*args, **kwargs)
    """
    objects = model.objects
    if excluded_obj and excluded_obj.pk:
        if not isinstance(excluded_obj, model):
            mes = ('"{}" class of parameter "excluded_obj" is not equal with model class ("{}"). '
                   '"excluded_obj" must be same class type as model.')
            raise ImproperlyConfigured(mes.format(excluded_obj._meta.object_name, model._meta.object_name))

        objects = objects.exclude(pk=excluded_obj.pk)
    repeat = True
    count = 0
    count_str = ''
    while repeat:
        slug = '{}{}'.format(checked_slug, count_str)
        if objects.filter(**{par: slug}).count() == 0:
            return slug

        count += 1
        count_str = '-{}'.format(count)


def get_unique_random_seq(model, par, excluded_obj=None, value=5, only_digit=False):
    """
    Examples:

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = get_unique_random_seq(self._meta.model, 'token', None, 40)

        return super(Subscriber, self).save(*args, **kwargs)
    """
    objects = model.objects
    if excluded_obj and excluded_obj.pk:
        if not isinstance(excluded_obj, model):
            mes = ('"{}" class of parameter "excluded_obj" is not equal with model class ("{}"). '
                   '"excluded_obj" must be same class type as model.')
            raise ImproperlyConfigured(mes.format(excluded_obj._meta.object_name, model._meta.object_name))

        objects = objects.exclude(pk=excluded_obj.pk)

    count = 0
    max_count = 10000000000000000
    while count < max_count:
        field_value = get_random_seq(value, only_digit)
        if objects.filter(**{par: field_value}).count() == 0:
            return field_value

        count += 1

    raise Exception('get_unique_random_seq loops infinitely')
