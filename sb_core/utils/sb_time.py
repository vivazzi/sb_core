import time


def print_elapsed_time(func):

    def wrapper(*args, **kw):
        start = int(round(time.time() * 1000))
        result = func(*args, **kw)
        end = int(round(time.time() * 1000))

        print(f'{func.__name__}: {end - start} ms')
        return result

    return wrapper
