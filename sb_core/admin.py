from django.conf import settings
from django.contrib import admin
from django.utils.translation import gettext_lazy as _


admin.site.site_title = 'Администрирование: {}'.format(settings.COMPANY_NAME)
admin.site.site_header = _('User settings')
