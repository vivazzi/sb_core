from datetime import timedelta
from io import BytesIO

from PIL import Image
from django import forms
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.forms import CheckboxInput
from django.template.loader import render_to_string
from django.utils.translation import gettext as _
from django.utils.safestring import mark_safe
from file_resubmit.admin import AdminResubmitFileWidget, AdminResubmitImageWidget

from sb_core import settings as app_settings
from sb_core.settings import PIC_FIELD_OPTIMIZE_PARS
from sb_core.utils.thumbnail import get_thumbnail_html
from django_select2.forms import Select2Widget as BaseSelect2Widget


class ColorPickerWidget(forms.widgets.TextInput):
    class Media:
        css = {'all': ['sb_color_picker/sb_color_picker.css', ]}
        js = [app_settings.JQUERY_URL, 'sb_core/js/admin_compat.js', 'sb_color_picker/sb_color_picker.js', ]

    def render(self, name, value, attrs=None, renderer=None):
        input_html = super(ColorPickerWidget, self).render(name, value, attrs, renderer)
        context = {'name': name,
                   'color': value or '#000',
                   'old_color': value,
                   'input_html': input_html}
        return render_to_string('widgets/color_picker.html', context)


class FileWidget(AdminResubmitFileWidget):
    pass


class PicWidget(AdminResubmitImageWidget):
    def __init__(self, attrs=None, w=None, h=None, th_type='contain', attname='pic'):
        super(PicWidget, self).__init__(attrs)
        self.w = w
        self.h = h
        self.th_type = th_type
        self.attname = attname

    def render(self, name, value, renderer=None, attrs=None):
        text_input_html = super(PicWidget, self).render(name, value, renderer, attrs)
        return mark_safe(''.join([text_input_html, self.get_thumb(value)]))

    def get_thumb(self, value):
        if self.is_initial(value):
            defaults = {'pic': self.attname}
            if self.w: defaults.update({'w': self.w})
            if self.h: defaults.update({'h': self.h})
            return mark_safe(get_thumbnail_html(value.instance, th_type=self.th_type, **defaults))

        return ''

    class Media:
        css = {'all': ['sb_core/css/pic_field.css', ]}


class ExtendedPicWidget(PicWidget):

    def __init__(self, attrs=None, w=None, h=None, th_type='contain', attname='pic'):
        super().__init__(attrs, w, h, th_type, attname)

    @property
    def use_optimize_name_field(self):
        return f'use_optimize_{self.attname}'

    def render(self, name, value, renderer=None, attrs=None):
        text_input_html = super(PicWidget, self).render(name, value, renderer, attrs)

        pic_html = ''
        if self.is_initial(value):
            pic_html = render_to_string('widgets/pic.html', {'thumb': self.get_thumb(value), 'url': value.url, 'name': self.attname})

        checked = PIC_FIELD_OPTIMIZE_PARS['checked_by_default']
        compress_field_html = CheckboxInput().render(self.use_optimize_name_field, None,
                                                     {'checked': checked, 'id': 'id_use_optimize'})

        help_text = (
            f'<div class="help">Оригинал изображения будет уменьшен до {PIC_FIELD_OPTIMIZE_PARS["max_size"]["width"]} пикс. в ширину '
            f'и {PIC_FIELD_OPTIMIZE_PARS["max_size"]["height"]} пикс. в высоту.'
            f'Качество изображения: {PIC_FIELD_OPTIMIZE_PARS["quality"]}%. Формат изображения: JPEG<br/>'
            f'Данная функция применяется только при сохранении нового загруженного изображения.'
            f'</div>'
        )

        compress_field_html = (
            '<div style="margin-top:20px;margin-bottom:20px;">'
            f'{compress_field_html} <label class="vCheckboxLabel" for="id_use_optimize">Использовать сжатие картинки?</label>'
            f'{help_text}'
            '</div>'
        )

        return mark_safe(''.join([text_input_html, pic_html, compress_field_html]))

    def value_from_datadict(self, data, files, name):
        value = super(ExtendedPicWidget, self).value_from_datadict(data, files, name)
        if value:
            use_optimize = CheckboxInput().value_from_datadict(data, files, self.use_optimize_name_field)
            if use_optimize:
                name, ext = value.name.rsplit('.', 1)
                if ext != 'svg':
                    with Image.open(value) as new_pic:
                        new_pic.thumbnail(
                            (PIC_FIELD_OPTIMIZE_PARS['max_size']['width'], PIC_FIELD_OPTIMIZE_PARS['max_size']['height']),
                            resample=Image.Resampling.LANCZOS,
                        )
                        pic_file = BytesIO()

                        attrs = {
                            'optimize': True
                        }
                        if new_pic.mode in ('RGB', 'L', 'CMYK'):
                            attrs.update({'format': 'JPEG', 'quality': PIC_FIELD_OPTIMIZE_PARS['quality']})
                        else:
                            attrs.update({'format': 'PNG', 'quality': 100})  # 100 is the longest by time, but the most compressed

                        new_pic.save(pic_file, **attrs)

                        pic_file.seek(0)
                        value = InMemoryUploadedFile(pic_file, getattr(value, 'field_name', None), f'{name}.jpg', 'image/jpeg', pic_file.getbuffer().nbytes, None)

        return value

    class Media:
        css = {'all': ['fancyBox/source/jquery.fancybox.css', ]}
        js = [app_settings.JQUERY_URL, 'sb_core/js/admin_compat.js', 'fancyBox/source/jquery.fancybox.pack.js', ]


class PicURLWidget(forms.widgets.URLInput):
    def __init__(self, attrs=None, w=None, h=None):
        super(PicURLWidget, self).__init__(attrs)
        self.w = w
        self.h = h

    def render(self, name, value, attrs=None, renderer=None):
        text_input_html = super(PicURLWidget, self).render(name, value, attrs, renderer)
        ctx = {'url': value, 'name': name, 'w': self.w, 'h': self.h}
        pic_html = render_to_string('widgets/url_pic.html', ctx)
        return mark_safe('{} {}'.format(text_input_html, pic_html))

    class Media:
        css = {'all': ['fancyBox/source/jquery.fancybox.css', ]}
        js = [app_settings.JQUERY_URL, 'sb_core/js/admin_compat.js', 'fancyBox/source/jquery.fancybox.pack.js', ]


class DurationWidget(forms.widgets.MultiWidget):

    def __init__(self, *args, **kwargs):
        widgets = (forms.NumberInput(), forms.NumberInput(), forms.NumberInput())
        super(DurationWidget, self).__init__(widgets, *args, **kwargs)

    def decompress(self, value):
        if value:
            return value.split(':')
        return [None, None, None]

    def format_output(self, rendered_widgets):
        return '<div class="duration_widget"><span>{} {}</span> <span>{} {}</span> <span>{} {}</span></div>'.format(
            rendered_widgets[0], _('h'), rendered_widgets[1], _('m'), rendered_widgets[2], _('sec')
        )

    def value_from_datadict(self, data, files, name):
        def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                return False

        duration = [widget.value_from_datadict(data, files, name + '_%s' % i) for i, widget in enumerate(self.widgets)]
        if not any(duration):
            return None

        duration = [item or 0 for item in duration]

        try:
            return timedelta(hours=float(duration[0])) + timedelta(minutes=float(duration[1])) + timedelta(seconds=float(duration[2]))
        except ValueError:
            return None

    class Media:
        css = {'all': ['duration_widget/duration_widget.css', ]}


class Select2Widget(BaseSelect2Widget):
    """
    Added jquery to widget
    """
    def _get_media(self):
        media = super(Select2Widget, self)._get_media()

        css = ['select2/css/select2.css']
        css.extend(media._css['screen'])

        js = ['admin/js/vendor/jquery/jquery.min.js', 'admin/js/jquery.init.js', 'sb_core/js/admin_compat.js']
        js.extend(media._js)

        return forms.Media(js=js, css={'screen': css})

    media = property(_get_media)
