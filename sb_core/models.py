import os
from copy import copy

from django.conf import settings
from django.core.exceptions import SuspiciousFileOperation
from django.db import models
from django.db.models import Max
from os.path import dirname, join, basename

from django.utils.safestring import mark_safe

from sb_core.fields import FileField, PicField
from sb_core.folder_mechanism import delete_folder, generate_folder, copy_file_changing_folder, change_folder
from sb_core.utils.sb_utils import transliterate
from sb_core.utils.os_utils import delete_file_if_change
from sb_core.utils.thumbnail import get_thumbnail_url, get_thumbnail_html


class classproperty(object):
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner, cls):
        return self.fget(cls)


class FileModel(models.Model):
    folder = models.CharField(max_length=5, editable=False, blank=True)

    def file_fields(self):
        return [field.name for field in self._meta.fields if issubclass(type(field), (FileField, PicField))]

    file_fields = property(file_fields)

    def upload_to(cls):
        app = cls._meta.app_label
        res = []
        for i, char in enumerate(cls._meta.object_name):
            if i != 0 and char.isupper():
                res.append('_')
            res.append(char)
        upload_to = ''.join(res).lower()
        if upload_to[-1] == 'y': return '{}_{}ies'.format(app, upload_to[:-1])
        elif upload_to[-1] in ('s', 'x', 'h'): return '{}_{}es'.format(app, upload_to)
        else: return '{}_{}s'.format(app, upload_to)

    upload_to = classproperty(upload_to)

    def main_file_field(self):
        return self.file_fields[0]

    main_file_field = property(main_file_field)

    def sync_file_fields(self):
        return self.file_fields

    sync_file_fields = property(sync_file_fields)

    def get_main_file(self):
        return getattr(self, self.main_file_field)

    def get_file(self, f=''):
        if not f: return self.get_main_file()
        else: return getattr(self, f)

    def get_files(self, synced=False):
        if synced:
            return [getattr(self, f) for f in self.sync_file_fields]

        return [getattr(self, f) for f in self.file_fields]

    def file_exists(self, f=''):
        f = self.get_file(f)
        try:
            return bool(f) and os.path.exists(f.path)
        except SuspiciousFileOperation:
            return False

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if hasattr(self, 'order') and not self.order: self.order = 1

        if not self.folder:
            self.folder = generate_folder(os.path.join(settings.MEDIA_ROOT, self.upload_to), 5)

        if self.id:
            try:
                old_obj = self.__class__.objects.get(id=self.id)

                for i, old_file in enumerate(old_obj.get_files(synced=True)):
                    new_file = getattr(self, self.file_fields[i])
                    deleted = delete_file_if_change(old_file, new_file)
                    if deleted and self.folder: delete_folder(old_file.path, self.folder)

            except self.__class__.DoesNotExist:
                pass

        files = self.get_files()
        for f in files:
            f.name = transliterate(f.name)
        super(FileModel, self).save(force_insert, force_update, using, update_fields)

    def remove_folder(self, f=''):
        if self.file_exists(f):
            delete_folder(self.get_file(f).path, self.folder)

    class Meta:
        abstract = True


class PicModel(FileModel):
    thumb_size = (135, 100)

    def thumbnail_url(self, pic=''):
        pic = self.get_file(pic)
        return get_thumbnail_url(pic.url)

    def thumb(self, pic='', **kwargs):
        defaults = {'th_type': 'contain'}
        defaults.update(kwargs)
        return mark_safe(get_thumbnail_html(self, pic=pic, **defaults))
    thumb.short_description = 'Миниатюра'

    def remove_all_thumbs(self):
        for file_field in self.file_fields:
            self.remove_thumbs(file_field)

    def get_thumb_paths(self, f=''):
        if self.file_exists(f):
            pic = self.get_file(f)
            if type(pic.field) == PicField:
                folder = dirname(pic.path)
                return [join(folder, f) for f in os.listdir(folder) if f != basename(pic.name)]

    def remove_thumbs(self, f=''):
        if self.file_exists(f):
            pic = self.get_file(f)
            if type(pic.field) == PicField:
                folder = dirname(pic.path)
                [os.remove(join(folder, f)) for f in os.listdir(folder) if f != basename(pic.name)]

    class Meta:
        abstract = True


def _copy_relations_handler(obj, old_instance):
    obj.folder = generate_folder(os.path.join(settings.MEDIA_ROOT, obj.upload_to), 5)

    for i, old_file in enumerate(old_instance.get_files()):
        file_field = obj.file_fields[i]
        if bool(old_file):
            f_postfix = file_field if len(old_instance.get_files()) >= 2 else ''
            setattr(obj, file_field, copy_file_changing_folder(old_file, obj.folder, f_postfix))
            change_folder(old_file.path)

    obj.save()
    [change_folder(f.path, reverse=True) for f in old_instance.get_files() if bool(f)]

    from sb_core.watermark_support import watermark_support
    watermark_support(obj, old_instance)


def copy_relations_fk(self, old_instance, fk, related_name_for_files='pics'):
    map_objects = []
    for obj in getattr(old_instance, related_name_for_files).all():
        old_obj = copy(obj)
        obj.id = None
        setattr(obj, fk, self)
        _copy_relations_handler(obj, old_obj)

        map_objects.append((obj, old_obj))

    return map_objects


class SEOMixin(models.Model):
    meta_tags = models.CharField(
        'Мета-тег keywords - ключевые слова', max_length=255, blank=True,
        help_text='Список тегов (ключевых слов), разделённых запятыми. '
                  'Рекомендуемое количество слов - до 10, общее количество символов - 150 символов.<br>'
                  'Например: создание сайтов, разработка сайтов, изготовление сайтов,'
    )

    meta_desc = models.CharField('Мета-тег: description - описание', max_length=255, blank=True,
                                 help_text='Краткое описание страницы. Рекомендуемая длина - 120-160 символов.<br>'
                                           'Например: Разработка сайтов любой сложности по России.')

    class Meta:
        abstract = True


class OGMixin(models.Model):
    og_title = models.CharField(
        'Название ссылки', max_length=250, blank=True,
        help_text='Если не указано, будет использоваться Название страницы '
                  '(см. в меню: "Страница \ Настройки страницы..."). Рекомендуется 50 символов.'
    )

    og_desc = models.CharField(
        'Описание ссылки', max_length=300, blank=True,
        help_text='Если не указано, будет использоваться "Мета-тег Description" страницы '
                  '(см. в меню: "Страница \ Настройки страницы..."). Допустимо до 295 символов.'
    )

    og_image = PicField(
        'Картинка ссылки', blank=True,
        help_text='Если не указано, будет использоваться картинка по умолчанию. Рекомендуемые размеры картинки '
                  '968×504 пикс. Некоторые части картинки могут обрезаться для разных соц. сетей.'
    )

    class Meta:
        abstract = True


class SaveOrderMixin(object):
    fk_for_order = None  # Ex: 'variant.pics'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.fk_for_order:
            raise Exception('You need set "fk_for_order" for {}'.format(self.__class__))

        fk, related_name = self.fk_for_order.split('.')
        if not self.order:
            self.order = (getattr(getattr(self, fk), related_name).aggregate(Max('order'))['order__max'] or 0) + 1

        super(SaveOrderMixin, self).save(force_insert, force_update, using, update_fields)
