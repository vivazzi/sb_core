��    .      �  =   �      �  y   �  :   k     �     �     �  
   �     �     �     �  >   �     0     D     L     Y  +   \     �     �     �     �     �  	   �     �     �     �  
   �       1        L  &   U  P   |      �  i   �     X     r     �     �     �     �     �     �     �     �     �     �     �  �  �  �   �	  `   p
     �
     �
     �
       3   +     _  
   s  q   ~  !   �          #     C  P   J     �  $   �     �     �  !   �          1     Q     d     �  .   �  b   �     )  /   6  �   f  ?   �  �   7  6   �  +   +     W     \     l     u     ~     �     �  -   �     �     �     �         +              	                             )   *   %      -                (                               "         '      &   #             
                ,                  .      $      !              <p><strong>Test html message. This text must be bold</strong>.<br/>If it is true, then letter was received correctly.</p> A notification has already been sent to technical support. Add Change language to Default Error code Fields marked by star Go to Home If you think this is a mistake, then contact the site support. Insufficient rights Manager Next article No Not have permission to view the information Page Page not found Pages Previous Previous article Return to Return to home pagehome page Send Sending of file... Sending... Site program error Sorry, but there was a programming error on page. Template Template "{obj.template}" is not found The path in the address bar may be incorrect, or the page may have been deleted. The requested page was not found Try refresh the page later. If it does not help, try clearing your cookies and refreshing the site again. Unable to process request User settings Yes You are here day days from h is not found is required m prev_nextNext sec Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 <p><strong>Тестовое html письмо. Этот текст должен быть жирным</strong>.<br/>Если это так, значит письмо было получено правильно. В техническую поддержку уже отправлено уведомление. Добавить Сменить язык на По умолчанию Код ошибки Поля, отмеченные звёздочкой Перейти на Домой Если вы считаете это ошибкой, то обратитесь к поддержке сайта. Недостаточно прав Менеджер Следующая статья Нет Недостаточно прав для просмотра информации Страница Страница не найдена Страницы Предыдущая Предыдущая статья Вернуться на главную страницу Отправить Отправка файла... Отправка... Программная ошибка сайта Извините, но на странице возникла программная ошибка. Шаблон Шаблон "{obj.template}" не найден Возможно, неправильно указан путь в адресной строке или страница была удалена. Запрашиваемая страница не найдена Попробуйте обновить страницу позже. Если не помогает, попробуйте почистить куки и снова обновить сайт. Невозможно обработать запрос Настройки пользователя Да Вы здесь день дней из ч не найден , являются обязательными м Следующая сек 