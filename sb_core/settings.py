from django.conf import settings
from os.path import join

SBL = getattr(settings, 'SBL', {})

JQUERY_URL = getattr(settings, 'JQUERY_URL', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js')
AWESOME_URL = getattr(settings, 'AWESOME_URL', join(settings.STATIC_URL, 'font-awesome-4.3.0/css/font-awesome.min.css'))
ANGULAR_URL = getattr(settings, 'ANGULAR_URL', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.0.6/angular.min.js')

BACKUP_INTERVAL = getattr(settings, 'SB_BACKUP_INTERVAL', 24 * 3600)
BACKUP_TIMEOUT = getattr(settings, 'SB_BACKUP_TIMEOUT', 2 * 3600)
BACKUP_COUNT = getattr(settings, 'SB_BACKUP_COUNT', 1)

DB_DIR = settings.BASE_DIR / 'db'

PIC_FIELD_OPTIMIZE_PARS = getattr(settings, 'SB_PIC_FIELD_OPTIMIZE_PARS', {'checked_by_default': True,
                                                                           'max_size': {'width': 1366, 'height': 920},
                                                                           'quality': 85})

PROTOCOL = getattr(settings, 'PROTOCOL', 'http' if settings.DEBUG else 'https')
