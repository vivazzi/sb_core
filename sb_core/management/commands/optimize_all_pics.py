import PIL
from PIL import Image
from django.core.management.base import BaseCommand
from django.apps import apps

from sb_core.models import PicModel
from sb_core.settings import PIC_FIELD_OPTIMIZE_PARS


def handle_model(model):
    offset = 0
    delta_offset = 100
    objects = model.objects.all()
    objects_count = objects.count()

    while offset < objects_count:
        print(f'INFO: {model._meta.app_label}.{model._meta.object_name}, offset = {offset}')
        for obj in objects[offset:offset+delta_offset]:
            for field_name in obj.file_fields:
                if obj.file_exists(field_name):
                    field = obj.get_file(field_name)

                    if getattr(field, 'is_svg', None) and not field.is_svg():
                        try:
                            with PIL.Image.open(field) as new_pic:
                                new_pic.thumbnail(
                                    (PIC_FIELD_OPTIMIZE_PARS['max_size']['width'], PIC_FIELD_OPTIMIZE_PARS['max_size']['height']),
                                    resample=Image.Resampling.LANCZOS,
                                )

                                attrs = {
                                    'optimize': True
                                }
                                if new_pic.mode in ('RGB', 'L', 'CMYK'):
                                    attrs.update({'format': 'JPEG', 'quality': PIC_FIELD_OPTIMIZE_PARS['quality']})
                                else:
                                    attrs.update({'format': 'PNG', 'quality': 100})  # 100 is the longest by time, but the most compressed

                                new_pic.save(field.path, **attrs)
                                new_pic.close()
                        except PIL.UnidentifiedImageError:
                            print(f'MISS: PIL.UnidentifiedImageError = {field}')
                            continue
                    else:
                        print(f'MISS: no has is_svg() method = {field}')

        offset += delta_offset


class Command(BaseCommand):
    def handle(self, *args, **options):
        for model in apps.get_models():
            if issubclass(model, PicModel):
                handle_model(model)

        print('\nDone!')
