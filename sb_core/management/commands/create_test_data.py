import shutil

from cms.models import Page
from django.conf import settings
from django.core.management.base import BaseCommand
from os.path import join, exists

from sb_picture.models import SBPicture
from sb_gallery.models import SBGalleryPicture

TEST_DATA_PATH = join(settings.MEDIA_ROOT, 'sb_test_data')


def get_doc(slug, title):
    link = ('<p>Подробнее о компоненте: <a href="https://vits.pro/info/components/base/{}" '
            'target="_blank">Документация компонента \"{}\"</a></p>')
    return link.format(slug, title)


class Command(BaseCommand):
    def handle(self, *args, **options):
        from cms.api import create_page, add_plugin
        reverse_id = 'demo_test_doc_page'

        # --- deleting old demo data ---
        if exists(TEST_DATA_PATH):
            shutil.rmtree(TEST_DATA_PATH)

        page = Page.objects.filter(reverse_id=reverse_id)
        if page:
            page.delete()

        # --- creating ---
        if Page.objects.count() == 0:
            create_page('Главная', 'INHERIT', 'ru', slug='main', in_navigation=True, published=True)

        old_test_data_path = join(settings.STATIC_ROOT, 'sb_test_data')
        if not exists(old_test_data_path):
            raise Exception('sb_test_data folder is not found. You must call collectstatic command or copy sb_test_data to collect_static folder.')
        shutil.copytree(old_test_data_path, TEST_DATA_PATH)

        page = create_page('Демонстрационная страница', 'INHERIT', 'ru', slug=reverse_id.replace('_', '-'), in_navigation=True,
                           reverse_id=reverse_id)
        page.save()

        pl = page.placeholders.get(slot='page_content')

        add_plugin(pl, 'TextPlugin', 'ru',
                   body='<h2 style="text-align:center;">Добро пожаловать на демонстрационную страницу от Viva IT Studio!</h2>'
                        '<p>Система управления содержимым сайта Django CMS проста в использовании. Чтобы получить справку по управлению '
                        'сайтом вы можете найти раздел в меню "Помощь" и выбрать необходимый пункт.</p>'
                        '<p>На этой странице представлен базовый набор компонентов, который имеет каждый разрабатываемый нами сайт. '
                        'Смело редактируйте компоненты, перемещайте их или удаляйте - почувствуйте лёгкость в управлении сайтом!</p>')

        # - text -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Текст</h3>')
        mes = ('<p>Вы можете создавать текст с произвольным форматированием, добавляя <strong>жирность</strong>, '
               '<em>курсив</em>, <span style="color:#800080;">цвет теста</span>, <span style="background-color: rgb(255, 255, 0);"> '
               'цвет фона текста</span>&nbsp;и т. д. Компонент &quot;Текст&quot; позволяет добавлять в содержимое текста другие '
               'компоненты: картинка, ссылка, файл и т. д.</p>')
        add_plugin(pl, 'TextPlugin', 'ru', body=mes)
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('text', 'Текст'))

        # - sb_picture -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Картинка</h3>')
        add_plugin(pl, 'SBPicturePlugin', 'ru', pic='sb_test_data/sb_picture/0001/sb_picture_1.jpg', alignment=SBPicture.CENTER)
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('picture', 'Картинка'))

        # - sb_gallery -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Галерея</h3>')
        plugin = add_plugin(pl, 'SBGalleryPlugin', 'ru')

        SBGalleryPicture(gallery=plugin, pic='sb_test_data/sb_gallery/0001/sb_gallery_1.jpg', order=1).save()
        SBGalleryPicture(gallery=plugin, pic='sb_test_data/sb_gallery/0002/sb_gallery_2.jpg', order=2).save()
        SBGalleryPicture(gallery=plugin, pic='sb_test_data/sb_gallery/0003/sb_gallery_3.jpg', order=3).save()
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('gallery', 'Галерея'))

        # - sb_slider -
        try:
            from sb_slider.models import SBSliderPicture

            add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Слайдер</h3>')
            plugin = add_plugin(pl, 'SBSliderPlugin', 'ru')

            SBSliderPicture(slider=plugin, pic='sb_test_data/sb_slider/0001/sb_slider_1.jpg', order=1).save()
            SBSliderPicture(slider=plugin, pic='sb_test_data/sb_slider/0002/sb_slider_2.jpg', order=2).save()
            SBSliderPicture(slider=plugin, pic='sb_test_data/sb_slider/0003/sb_slider_3.jpg', order=3).save()
            add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('slider', 'Слайдер'))
        except ImportError:
            pass

        # - sb_link -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Ссылка / Кнопка</h3>')
        add_plugin(pl, 'SBLinkPlugin', 'ru', title='Руководство пользователя системой управления содержимым сайта Django CMS',
                   link='https://vits.pro/info/django-cms/')
        add_plugin(pl, 'SBLinkPlugin', 'ru', title='Перейти на главную', link='/', template='sb_link/btn.html')
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('link', 'Ссылка'))

        # - sb_file -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Файл</h3>')
        add_plugin(pl, 'SBFilePlugin', 'ru', title='Тестовый файл', file='sb_test_data/sb_file/0001/sb_file_1.txt')
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('file', 'Файл'))

        # - sb_cols -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Колонки</h3>')

        cols_plugin = add_plugin(pl, 'SBColsPlugin', 'ru', title='Тестовые колонки')
        col_1 = add_plugin(pl, 'SBColPlugin', 'ru', target=cols_plugin, width='33.33%')
        col_2 = add_plugin(pl, 'SBColPlugin', 'ru', target=cols_plugin, width='33.33%')
        col_3 = add_plugin(pl, 'SBColPlugin', 'ru', target=cols_plugin, width='33.33%')

        add_plugin(pl, 'TextPlugin', 'ru', target=col_1, body='<p>Первая колонка</p>')
        add_plugin(pl, 'TextPlugin', 'ru', target=col_1, body='<p>В колонку можно вставлять любой текст</p>')
        add_plugin(pl, 'TextPlugin', 'ru', target=col_2, body='<p>Вторая колонка</p>')
        add_plugin(pl, 'TextPlugin', 'ru', target=col_2, body='<p>В колонку можно вставлять и картинку:</p>')
        add_plugin(pl, 'SBPicturePlugin', 'ru', target=col_2, pic='sb_test_data/sb_picture/0002/sb_picture_2.jpg',
                   alignment=SBPicture.CENTER)
        add_plugin(pl, 'TextPlugin', 'ru', target=col_3, body='<p>Третья колонка</p>')
        add_plugin(pl, 'TextPlugin', 'ru', target=col_3, body='<p>В колонку можно вставлять любой компонент! Например, файл:</p>')
        add_plugin(pl, 'SBFilePlugin', 'ru', target=col_3, title='Тестовый файл', file='sb_test_data/sb_file/0002/sb_file_2.txt')
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('cols', 'Колонки'))

        # - sb_snippet -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Сниппет</h3>')
        content = ('<iframe width="560" height="315" src="https://www.youtube.com/embed/Ma3uxxH69uw?rel=0" '
                   'frameborder="0" allowfullscreen></iframe>')
        add_plugin(pl, 'SBSnippetPlugin', 'ru', content=content, is_video=True)
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('snippet', 'Сниппет'))

        # - sb_form_base -
        add_plugin(pl, 'TextPlugin', 'ru', body='<h3 style="text-align:center;">Форма обратной связи</h3>')
        add_plugin(pl, 'SBContactFormPlugin', 'ru', form='FeedBackForm')
        add_plugin(pl, 'TextPlugin', 'ru', body=get_doc('form', 'Форма обратной связи'))
