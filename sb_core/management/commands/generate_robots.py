from os.path import exists

from django.core.management.base import BaseCommand

from sb_core.constants import ROBOTS_PATH
from sb_core.views.seo_views import generate_default_robots_content


class Command(BaseCommand):
    help = 'Robots generation'

    def handle(self, *args, **options):
        if not exists(ROBOTS_PATH):
            generate_default_robots_content()
