from django.core.management.base import BaseCommand

from sb_core import settings
from sb_core.views import generate_sitemap_handler, my_ping_google
from django.http import HttpRequest


class DummyUser:
    def __init__(self):
        self.is_staff = False


class DummyHttpRequest(HttpRequest):
    # for django-shop
    customer = 'Fake'
    user = DummyUser()
    # end: for django-shop

    def _get_scheme(self):
        return settings.PROTOCOL


class Command(BaseCommand):
    help = 'Sitemap generation'

    def handle(self, *args, **options):
        request = DummyHttpRequest()

        generate_sitemap_handler(request)
        my_ping_google()
