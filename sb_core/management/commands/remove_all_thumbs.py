from django.core.management import call_command
from django.core.management.base import BaseCommand

from sb_core.utils.thumbnail import remove_all_thumbs_from_models


class Command(BaseCommand):
    def handle(self, *args, **options):
        call_command('thumbnail_cleanup')
        remove_all_thumbs_from_models()
