$(function(){
    var $outer_slides = $('.slider .outer_slide');

    $outer_slides.click(function(){
        var $parent_slider = $(this).parents('.slider');
        var $parent_slides = $parent_slider.find('.slide');

        $parent_slides.removeClass('current_slide');
        $parent_slider.find('.outer_slide').removeClass('current_outer_slide');

        $(this).addClass('current_outer_slide');

        var id = $(this).attr('data-id');
        var $current_slide = $parent_slides.filter('[data-id='+id+']');
        $current_slide.addClass('current_slide');

        var $img = $current_slide.find('img');
        var data_src = $img.data('src');
        if (data_src) {
            $img.attr('src', data_src).removeAttr('data-src').attr('srcset', $img.data('srcset')).removeAttr('data-srcset');
            $img.load(function() {
                $img.addClass('b-loaded');
            })
        }

        var $a = $(this).parents('.item').find('a');

        var get_pars = window.location.search;

        if ($(this).index() == 0) $a.attr({'href': $parent_slider.data('url')+get_pars});
        else {
            get_pars += (get_pars) ? '&' : '?';
            $a.attr({'href': $parent_slider.data('url')+get_pars+$parent_slider.data('par')+'='+$(this).data('color_id_for_get_pars')});
        }

        return false;
    });
});