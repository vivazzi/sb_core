$(function(){
    $('.sb_detail_slider .item').click(function(){
        var $current_slider = $(this).parents('.sb_detail_slider');

        var id = $(this).attr('data-id');

        var $current_slide = $current_slider.find('.slide').removeClass('active').filter('[data-id='+id+']');
        $current_slide.addClass('active');
        var $img = $current_slide.find('img');
        var data_src = $img.data('src');
        if (data_src) {
            $img.attr('src', data_src).removeAttr('data-src').attr('srcset', $img.data('srcset')).removeAttr('data-srcset');
            $img.load(function() {
                $current_slide.removeClass('loading');
            })
        }

        $current_slider.find('.item').removeClass('active').filter('[data-id='+id+']').addClass('active');

        return false;
    });
});