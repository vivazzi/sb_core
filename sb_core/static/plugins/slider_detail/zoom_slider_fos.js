$(function(){
    var $body = $('body');
    $('.sb_detail_slider.zs.fos .slide').click(function(event) {
        $($body).addClass('overflow_locker');

        var $zoom_slider = $('<div class="zoom_slider fos"></div>');
        $body.append($zoom_slider);

        // adding slider to zoom_slider
        var url = "url('"+$(this).data('url')+"')";
        var $zoom_slide = $('<div class="slide" style="background-image: '+url+'"></div>');

        $zoom_slider.append($zoom_slide);

        $zoom_slide.click(function () {
            $($body).removeClass('overflow_locker');
            $(this).parents('.zoom_slider').remove();
        });

        // mouse moving for pic moving
        percent_x = (event.clientX / $(window).width()) * 100;
        percent_y = (event.clientY / $(window).height()) * 100;
        $zoom_slide.css({'background-position': percent_x + '% ' + percent_y + '%'});

        var percent_x, percent_y;
        $zoom_slider.mousemove(function (event){
            if (!isNaN(event.clientX)){
                percent_x = (event.clientX / $(window).width()) * 100;
                percent_y = (event.clientY / $(window).height()) * 100;
                $zoom_slide.css({'background-position': percent_x + '% ' + percent_y + '%'});
            }
        });
    })
});