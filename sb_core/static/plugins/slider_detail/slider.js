$(function(){
    $('.sb_detail_slider .item').click(function(){
        var $current_slider = $(this).parents('.sb_detail_slider');

        var id = $(this).attr('data-id');
        $current_slider.find('.slide').removeClass('active').filter('[data-id='+id+']').addClass('active');
        $current_slider.find('.item').removeClass('active').filter('[data-id='+id+']').addClass('active');

        return false;
    });
});