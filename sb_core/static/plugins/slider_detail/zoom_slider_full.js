$(function(){
    var $body = $('body');
    $('.sb_detail_slider.zs.full .slide').click(function(event) {
        $($body).addClass('overflow_locker');

        var $zoom_slider = $('<div class="zoom_slider full"></div>');
        $body.append($zoom_slider);
        // $zoom_slider.animate({'opacity': 1}, 150);

        var id = $(this).attr('data-id');


        // adding slider to zoom_slider
        var $all_slides = $(this).parents('.w').find('.slide');
        $all_slides.each(function (index) {
            var url = "url('"+$(this).data('url')+"')";
            var $zoom_slide = $('<div class="slide" data-id="'+(index+1)+'" style="background-image: '+url+'"></div>');

            if (index + 1 == id) $zoom_slide.addClass('active');
            $zoom_slider.append($zoom_slide);

            $zoom_slide.click(function () {
                $($body).removeClass('overflow_locker');
                $(this).parents('.zoom_slider').remove();
            });
        });


        // adding left and right navigation
        var $nav_left = $('<div class="side side_l"><i class="fa fa-angle-left navigation nav_left"></i></div>');
        var $nav_right = $('<div class="side side_r"><i class="fa fa-angle-right navigation nav_right"></i></div>');
        $zoom_slider.append($nav_left);
        $zoom_slider.append($nav_right);

        var $nav = $nav_left.add($nav_right);
        var $zoom_slide = $zoom_slider.find('.slide');
        $nav.click(function(){
            var direction = 'right';
            if ($nav.hasClass('.nav_left')) direction = 'left';

            var active_id = parseInt($zoom_slide.filter('.active').attr('data-id'));
            $zoom_slide.removeClass('active');
            if (direction == 'right') {
                active_id++;
                if (active_id == $zoom_slide.length + 1) active_id = 1;
            } else {
                active_id--;
                if (active_id == 0) active_id = $zoom_slide.length;
            }

            $zoom_slide.filter('[data-id='+active_id+']').addClass('active').show();
        });


        // mouse moving for pic moving
        percent_x = (event.clientX / $(window).width()) * 100;
        percent_y = (event.clientY / $(window).height()) * 100;
        $zoom_slide.css({'background-position': percent_x + '% ' + percent_y + '%'});

        var percent_x, percent_y;
        $zoom_slider.mousemove(function (event){
            if (!isNaN(event.clientX)){
                percent_x = (event.clientX / $(window).width()) * 100;
                percent_y = (event.clientY / $(window).height()) * 100;
                $zoom_slide.css({'background-position': percent_x + '% ' + percent_y + '%'});
            }
        });
    })
});