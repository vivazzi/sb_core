/* SBL: helper library (alignments.js)
 * Copyright (c) 2013 - 2016 Maltsev Artem, maltsev.artjom@gmail.com, vivazzi.pro
 * web studio: Viva IT Studio, vits.pro
 */
function alignment_of_image($image, $image_window){
    // Alignment of only <img> tags using margin
    function alignment($image, $window){
        var window_aspect_ratio = $window.width()/$window.height();
        var image_aspect_ratio = $image.width()/$image.height();

        if (image_aspect_ratio < window_aspect_ratio) $image.css({width: '100%', 'margin-top': ($window.height()-$window.width()/image_aspect_ratio)/2});
        else $image.css({height: '100%', 'margin-left': ($window.width()-$window.height()*image_aspect_ratio)/2})
    }
    $image.css({width: 'auto', height: 'auto', 'margin-top': 'auto', 'margin-left': 'auto'});

    if ($image.width() == 0){
        $image.load(function(){
            $(this).removeAttr('width').removeAttr('height').css({ width: '', height: '' });
            alignment($(this), $image_window);
            if ($image.css('opacity') == '0') $image.animate({opacity: 1}, 200);
        });
    } else {
        alignment($image, $image_window);
        if ($image.css('opacity') == '0') $image.animate({opacity: 1}, 200);
    }
}


function alignment_of_background_image($image, image_width, image_height,  $image_window){
    // Alignment of block tags (not <img>, see alignment_of_image) use background-position
    // example:
    // $.each($images, function(){
    //     alignment_of_background_image($(this), $(this).attr('data-width'), $(this).attr('data-height'), $(this).parent())
    // });
    function alignment($image, image_width, image_height, $window){
        var window_aspect_ratio = $window.width()/$window.height();
        var image_aspect_ratio = image_width/image_height;

        if (image_aspect_ratio < window_aspect_ratio) $image.css({'background-position': '0 '+($window.height()-$window.width()/image_aspect_ratio)/2+'px'});
        else $image.css({'background-position': ($window.width()-$window.height()*image_aspect_ratio)/2+'px 0'})
    }
    $image.css({width: '100%', height: '100%'});

    alignment($image, image_width, image_height, $image_window);
    if ($image.css('opacity') == '0') $image.animate({opacity: 1}, 200);
}

function alignment_of_absolute_image($image, $image_window){
    // Alignment of only <img> using
    // top и left, which is inside wrapper/window with 'position:relative' property
}