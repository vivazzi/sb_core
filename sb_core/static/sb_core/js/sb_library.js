/* SBL: helper library (sb_library.js)
 * Copyright (c) 2013 - 2016 Maltsev Artem, maltsev.artjom@gmail.com, vivazzi.pro
 * web studio: Viva IT Studio, vits.pro
 */
function parseJSON(parJSON){
    // check on string for safety
    if (typeof parJSON == 'string'){
        return window.JSON && window.JSON.parse ? JSON.parse(parJSON) : eval('('+parJSON+')'); //if browser support built-in JSON, then use it. Else (IE6-7) use 'eval'
    }
    return parJSON;
}

function active_toggle(manager_el, toggle_elements, active_on_checked, use_slide, hor_filter_elements){
    function hide_toggle_elements(to_hide) {
        var el, selector, name;
        $.each(toggle_elements, function () {
            el = '';
            name = $(this).attr('name');

            // check on hor_filter_elements
            $.each(hor_filter_elements, function (index, value) {
                if (name == value + '_old') el = $('.field-'+value);
            });

            if (!el) el = $(this).parents('.field-'+name);

            if (to_hide) el.hide(); else el.slideToggle(500);
        });
    }

    function do_enable() {
        if (!use_slide) toggle_elements.removeAttr('disabled');
    }

    function do_disable(use_hide) {
        if (!use_slide) toggle_elements.attr('disabled','disabled');
        if (use_hide) hide_toggle_elements(true);
    }

    if (use_slide == undefined) use_slide = false;
    if (hor_filter_elements == undefined) hor_filter_elements = false;

    if (active_on_checked) {
        if (manager_el.attr('checked')) do_enable(); else do_disable(use_slide);
    } else {
        if (!manager_el.attr('checked')) do_enable(); else do_disable(use_slide);
    }

    manager_el.click(function(){
        if (toggle_elements.attr('disabled') == 'disabled') do_enable(); else do_disable(false);

        if (use_slide) hide_toggle_elements(false);
    });
}

function with_end(number, array){
    array = array.split('_');
    var result = array[2];
    var i = number % 100;
    if (11 <= number && number <= 19) {
        result = array[2]
    } else {
        i = i % 10;
        if (i == 1) result = array[0];
        if (1 < i && i <= 4) result = array[1];
        if (i > 4) result = array[2];
    }
    return number + ' ' + result
}