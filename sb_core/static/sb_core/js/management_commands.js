$(function() {
    let $command_select = $('#id_command');
    let $item = $('.commands .item').hide();

    $item.filter('[data-cmd="'+$command_select.val()+'"]').show();

    $command_select.change(function () {
        $item.hide().filter('[data-cmd="'+$(this).val()+'"]').show()
    });
});