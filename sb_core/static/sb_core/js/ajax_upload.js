function log($log, html, mes_type) {
    if (html != undefined){
        if (mes_type == undefined) mes_type = 'i';
        $log.html('<div class="mes '+mes_type+'">' + html + '</div>').show();
    } else $log.hide();
}

function upload($form, callback_success) {
    var url = $form.data('url');
    var $log = $form.find('.log');
    var $progress = $form.find('.progress');
    var $loader = $form.find('.loader');

    var $error = $form.find('.error');
    var $error_list = $form.find('.error_list');
    var $item_row = $form.find('.item_row');
    var $all_fields = $form.find('input, select, textarea');

    $all_fields.removeClass('error_field');
    $error.hide();
    $error_list.hide();

    var xhr = new XMLHttpRequest();

    xhr.onload = xhr.onerror = function(data) {
        if (this.status == 200) {
            data = JSON.parse(this.responseText);
            if (data.errors) {
                for (var error_item in data.errors) {
                    if (error_item == '__all__'){
                        var html = '';
                        for (var item in data.errors[error_item]) {
                            html += data.errors[error_item][item]
                        }
                        $form.find('.error_list').text(html).show();
                    } else {
                        var error_item_selector = error_item;

                        $item_row.find('.error').filter('[id$='+error_item_selector+']').html(data.errors[error_item][0]).show();
                        $all_fields.filter('[id$='+error_item_selector+']').addClass('error_field').focus();
                    }
                }
                log($log, data.message, 'e');
            }
            else {
                log($log, data.message);

                if (callback_success != undefined) callback_success(data);
            }
        } else {
            log($log, 'Error: ' + this.status, 'e');
        }
        $loader.removeClass('active');
    };

    xhr.upload.onprogress = function(event) {
        if ($loader.length != 0) {
            $loader.addClass('active');
        } else log($log, 'Идёт обработка. Пожалуйста, подождите...');

        if (event.loaded != event.total) {
            $progress.show().find('.v').css({'width': (event.loaded / event.total) * 100 + '%'});
        }
        else {
            $progress.hide().find('.v').css({'width': '0%'});
        }
    };

    xhr.open('POST', url, true);
    var formData = new FormData($form[0]);
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('X-REQUESTED-WITH', 'XMLHttpRequest');
    xhr.send(formData);
}