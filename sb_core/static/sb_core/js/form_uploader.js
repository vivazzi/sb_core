var formUploader = {
    prepareForm: function(form){
        // Каждая значимая кнопка формы при клике должна создать одноименное hidden поле,
        // чтобы на сервер передалась информация о том, какая кнопка была кликнута
        var allFormFields = form.getElementsByTagName('input');
        for (var i=0; i<allFormFields.length; i++){
            if(allFormFields[i].type == 'submit' && allFormFields[i].name){
                allFormFields[i].onclick = function(){
                    formUploader.createHiddenField(this);
                }
            }
        }

        // Визуализируем форму как отправляемую на сервер на событии onsubmit (в т.ч. делаем все кнопки неактивными)
        form.onsubmit = function(){
            formUploader.setFormLoading(form);
            return false;
        };

        // Очищаем визуализацию формы (в т.ч. делаем все кнопки вновь активными)
        // при уходе со страницы - по глобальному событию onunload
        window.onunload = function(){
            formUploader.clearFormLoading(form)
        }
    },

    setFormLoading: function(form){
        // Создаем визуализацию загрузки формы и делаем все кнопки неактивными disabled=true;
        var allFormFields = form.getElementsByTagName('input');
        for (var i=0; i<allFormFields.length; i++){
            if(allFormFields[i].type == 'submit' && allFormFields[i].name){
                allFormFields[i].disabled=true;
                allFormFields[i].value=form_uploader_lang.sending;
            }
        }
    },

    setFormFileLoading: function(form){
        // Создаем визуализацию загрузки формы и делаем все кнопки неактивными disabled=true;
        var allFormFields = form.getElementsByTagName('input');
        for (var i=0; i<allFormFields.length; i++){
            if(allFormFields[i].type == 'submit' && allFormFields[i].name){
                allFormFields[i].disabled=true;
                allFormFields[i].value=form_uploader_lang.file_sending;
            }
        }
    },

    clearFormLoading: function(form){
        // Очищаем форму от визуализации загрузки и возвращаем кнопки в активное состояние disabled=false;
        var allFormFields = form.getElementsByTagName('input');
        for (var i=0; i<allFormFields.length; i++){
            if(allFormFields[i].type == 'submit' && allFormFields[i].name){
                allFormFields[i].disabled=false;
                if (allFormFields[i].dataset.initial_title != undefined) allFormFields[i].value=allFormFields[i].dataset.initial_title;
                else allFormFields[i].value=form_uploader_lang.send;
            }
        }
    },

    createHiddenField: function(button){
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = button.name;
        input.value = button.value;
        button.parentNode.insertBefore(input, button);
    }
};