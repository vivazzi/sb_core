function get_ext(filename){
    function in_array(value, array){
        for(var i = 0; i < array.length; i++) if(array[i] == value) return true;
        return false;
    }
    var ext = filename.substr(filename.lastIndexOf('.')+1).toLowerCase();

    // if the extension is the same as the icon
    if (in_array(ext, ['jpg', 'pcx', 'tga', 'bmp', 'gif'])) return ext;

    // other cases
    if (in_array(ext, ['psd', 'psb'])) return 'psd';
    if (in_array(ext, ['doc', 'docx'])) return 'doc';
    if (in_array(ext, ['xls', 'xlsx'])) return 'xls';
    if (in_array(ext, ['ppt', 'pptx'])) return 'ppt';
    if (in_array(ext, ['pdf', 'djvu', 'epub'])) return 'pdf';
    if (in_array(ext, ['mov', 'qt'])) return 'quick_time';
    if (in_array(ext, ['zip', 'rar', 'tar', 'gz', 'jar'])) return 'zip';
    if (in_array(ext, ['bmp', 'jpeg', 'png', 'ico'])) return 'image';
    if (in_array(ext, ['mp3', 'wav'])) return 'audio';
    if (in_array(ext, ['avi', 'mp4', 'mts'])) return 'video';

    return 'txt';
}