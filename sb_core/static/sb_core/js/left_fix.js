var $img = $('.left_fix');
var $img_wr = $img.parent();

$(window).resize(function(){
    $img.each(function(){
        $img.css({'left': ($img_wr.width()-$img.width())/2});
    });
}).resize();

// ugly hack for set slider height
var resize_intervals = [100, 200, 1000];
for (var i in resize_intervals){
    setTimeout(function(){
        $(window).resize();
    }, resize_intervals[i]);
}

window.addEventListener('orientationchange', function() {
    $(window).resize();
});