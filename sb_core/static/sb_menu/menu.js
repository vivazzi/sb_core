$(function(){
    var $show_menu = $('.show_menu');
    var $menu = $('nav ul.menu');

    $($show_menu).on('click', function() {
        $menu.slideToggle(200);
    });

    $(window).resize(function(){
        if ($(window).width() > 991) $menu.removeAttr('style');
    });

    var $nav = $('nav');
    var $ul_level_1 = $nav.find('ul.menu > li > .parent_li').next();
    $nav.find('.parent_li').click(function(){
        if (window.innerWidth <= 991) {
            var $current_ul = $(this).next();
            var $parents_ul = $(this).parents('ul');

            $current_ul.slideToggle(200);
            if ($parents_ul.length == 1){
                var $ul_without_current = $ul_level_1.not($current_ul);
                $ul_without_current.slideUp(200);
            }
        }
    });
});