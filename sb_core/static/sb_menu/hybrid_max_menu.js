$(function(){
    var $body = $('body');
    var $nav = $('nav');
    var $show_menu = $('.show_menu');
    var $hide_menu = $('.hide_menu');
    var hidden = true;

    var $fade = null;
    $show_menu.click(function(){
        if (hidden) {
            if (!$fade) {
                $fade = $('<div class="fade"></div>');
                $body.append($fade);
                $fade.click(function() {
                    $(this).fadeOut(200);
                    $nav.animate({'left': -$nav.data('mobile_left')}, 200);
                    $body.css({'overflow': 'auto'});
                    hidden = !hidden;
                    $show_menu.removeClass('active');
                });
            } else $fade.fadeIn(200);

            $nav.animate({'left': 0}, 200);
            $body.css({'overflow': 'hidden'});
        }
        else {
            $fade.fadeOut(200);
            $nav.animate({'left': -$nav.data('mobile_left')}, 200);
            $body.css({'overflow': 'auto'});
        }

        $(this).toggleClass('active');
        hidden = !hidden;
    });

    $hide_menu.click(function () {
        $show_menu.click();
    });

    $(window).resize(function(){
        if (window.innerWidth > 991) {
            if (!hidden){
                $fade.hide();
                hidden = true;
            }
            $nav.removeAttr('style');
        }
    });

    var $ul_level_1 = $nav.find('ul.menu > li > .parent_li').next();
    $nav.find('.parent_li').click(function(){
        if (window.innerWidth <= 991) {
            var $current_ul = $(this).next();
            var $parents_ul = $(this).parents('ul');

            $current_ul.slideToggle(200);
            if ($parents_ul.length == 1){
                var $ul_without_current = $ul_level_1.not($current_ul);
                $ul_without_current.slideUp(200);
            }
        }
    });
});