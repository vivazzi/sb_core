$(function(){
    var $body = $('body');
    var $header = $body.find('header');
    var $nav_pl = $header.find('.nav_pl');
    var $nav = $nav_pl.find('nav');

    var init = false;
    var animating = false;
    var top = 0, old_top = 0;

    $(window).scroll(function(){
        if (window.innerWidth < 992){
            top = $(this).scrollTop();

            $body.css({'margin-top': $header.outerHeight(true)});

            if (init == false) {
                $header.css({top: 0, position: 'fixed'});
                $nav.appendTo($body);
                init = true
            }

            if (!animating){
                if (top < 200 || old_top > top) {
                    animating = true;
                    $header.stop().animate({'top': 0}, 100, function () {
                        animating = false
                    });
                } else {
                    animating = true;
                    $header.stop().animate({'top': -$header.outerHeight(true)}, 100, function () {
                        animating = false
                    })
                }
            }

            old_top = top;
        }
        else{
            if (init == true) {
                $body.css({'margin-top': 0});
                $header.css({top: 'auto', position: 'relative'});
                $nav.appendTo($nav_pl);
                init = false
            }
        }
    });

    $(window).resize(function(){
        $(window).scroll();
    }).resize();

    window.addEventListener('orientationchange', function() {
        $(window).resize();
    });
});