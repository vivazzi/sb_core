var $show_menu = document.getElementsByClassName('show_menu');
var $menu = document.getElementsByClassName('menu');

var i;
var len;

for(i=0, len=$show_menu.length; i<len; i++){
    var $current_menu = $menu[i];
    $show_menu[i].onclick = function() {
        $current_menu.classList.toggle('expended');
    };
}