from PIL import Image
from easy_thumbnails.processors import colorspace


def background(im, size, bg=None, ct=None, obj=None, f='', **kwargs):
    """
    Add borders of a certain color to make the resized image fit exactly within the dimensions given.

    bg:
        Background color to use

    Next parameters are used for signals only:
        - ct (content_type_id)
        - obj (object_id)
        - f (pic_field)
    """
    if not bg:
        # Primary option not given, nothing to do.
        return im
    if not size[0] or not size[1]:
        # One of the dimensions aren't specified, can't do anything.
        return im
    x, y = im.size
    if x >= size[0] and y >= size[1]:
        # The image is already equal to (or larger than) the expected size, so
        # there's nothing to do.
        return im

    if im.mode != 'RGBA':
        im = im.convert('RGBA')

    im = colorspace(im, replace_alpha=None, **kwargs)
    new_im = Image.new('RGBA', size)
    if new_im.mode != im.mode:
        new_im = new_im.convert(im.mode)
    offset = (size[0]-x)//2, (size[1]-y)//2
    new_im.paste(im, offset)
    return new_im
