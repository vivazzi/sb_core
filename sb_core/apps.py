from django.apps import AppConfig

from django.db.models.signals import post_delete, post_save
from django.utils.module_loading import autodiscover_modules

from sb_core.signals import generate_sitemap


def remove_folder(sender, instance, **kwargs):
    for sync_file in instance.sync_file_fields:
        instance.remove_folder(sync_file)


class SBCoreConfig(AppConfig):
    name = 'sb_core'
    verbose_name = 'SB Core'

    def ready(self):
        from django.apps import apps
        from sb_core.models import FileModel
        from django.contrib.sites.models import Site

        for model in apps.get_models():
            if issubclass(model, FileModel):
                post_delete.connect(remove_folder, sender=model)

        post_save.connect(generate_sitemap, sender=Site)

        autodiscover_modules('sb_templates')
