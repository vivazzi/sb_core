from datetime import timedelta
import sys
from html import entities
from io import BytesIO
from PIL import Image
import xml.etree.cElementTree as et
from xml.etree.ElementTree import XMLParser, iterparse

from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator, get_available_image_extensions
from django.db import models
from django.db.models.fields.files import FieldFile
from django.db.utils import ProgrammingError
from django.forms import Select, HiddenInput, ImageField as DjangoImageField

from sb_core.template_pool import template_pool
from sb_core.files.images import SvgImageFile
from sb_core.folder_mechanism import upload_to_handler
from sb_core.widgets import ColorPickerWidget, PicWidget, ExtendedPicWidget, FileWidget, PicURLWidget, DurationWidget


class TemplateField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 255
        super(TemplateField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        if not hasattr(self.model, 'pool'):
            self.model.pool = self.model.__name__

        templates = template_pool.templates[self.model.pool] if self.model.pool in template_pool.templates else {}
        if not templates:
            kwargs['widget'] = HiddenInput()
        else:
            choices = []
            if self.blank is True:
                choices.append(('', '---------'))

            choices.extend([(name, template.title) for name, template in templates.items()])

            kwargs['widget'] = Select(choices=choices)
        return super(TemplateField, self).formfield(**kwargs)


class ColorField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 30
        super(ColorField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        kwargs['widget'] = ColorPickerWidget
        return super(ColorField, self).formfield(**kwargs)


class FileField(models.FileField):
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 255

        if 'upload_to' not in kwargs:
            kwargs['upload_to'] = upload_to_handler

        super(FileField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        kwargs['widget'] = FileWidget
        return super(FileField, self).formfield(**kwargs)


class SvgImageFieldFile(SvgImageFile, FieldFile):
    def delete(self, save=True):
        # Clear the image dimensions cache
        if hasattr(self, '_dimensions_cache'):
            del self._dimensions_cache
        super(SvgImageFieldFile, self).delete(save)


def validate_image_file_extension(value):
    default_extensions = get_available_image_extensions()
    default_extensions.append('svg')
    return FileExtensionValidator(allowed_extensions=default_extensions)(value)


class SVGAndImageFormField(DjangoImageField):
    default_validators = [validate_image_file_extension]

    def to_python(self, data):
        """
        Checks that the file-upload field data contains a valid image (GIF, JPG,
        PNG, possibly others -- whatever the Python Imaging Library supports).
        """
        test_file = super(DjangoImageField, self).to_python(data)
        if test_file is None:
            return None

        # We need to get a file object for Pillow. We might have a path or we might
        # have to read the data into memory.
        if hasattr(data, 'temporary_file_path'):
            ifile = data.temporary_file_path()
        else:
            if hasattr(data, 'read'):
                ifile = BytesIO(data.read())
            else:
                ifile = BytesIO(data['content'])

        try:
            # load() could spot a truncated JPEG, but it loads the entire
            # image in memory, which is a DoS vector. See #3848 and #18520.
            image = Image.open(ifile)
            # verify() must be called immediately after the constructor.
            image.verify()

            # Annotating so subclasses can reuse it for their own validation
            test_file.image = image
            test_file.content_type = Image.MIME[image.format]
        except Exception:
            # add a workaround to handle svg images
            if not self.is_svg(ifile):
                raise Exception(self.error_messages['invalid_image'])
                # six.reraise(ValidationError, ValidationError(
                #     self.error_messages['invalid_image'],
                #     code='invalid_image',
                # ), sys.exc_info()[2])
        if hasattr(test_file, 'seek') and callable(test_file.seek):
            test_file.seek(0)
        return test_file

    def is_svg(self, f):
        """
        Check if provided file is svg
        """
        f.seek(0)
        tag = None
        try:
            parser = XMLParser()
            # parser.parser.UseForeignDTD(True)
            # parser.entity = {'umml': chr(entities.name2codepoint['uuml'])}

            for event, el in iterparse(f, events=['start'], parser=parser):
                tag = el.tag
                break
        except et.ParseError:
            pass
        return tag == '{http://www.w3.org/2000/svg}svg'


class PicField(models.ImageField):
    attr_class = SvgImageFieldFile

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 255
        if 'upload_to' not in kwargs:
            kwargs['upload_to'] = upload_to_handler

        self.with_fb = kwargs.pop('with_fb', True)
        self.th_type = kwargs.pop('th_type', 'contain')
        self.w = kwargs.pop('w', None)
        self.h = kwargs.pop('h', None)
        super(PicField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'th_type': self.th_type, 'w': self.w, 'h': self.h, 'attname': self.attname}
        kwargs['widget'] = ExtendedPicWidget(**defaults) if self.with_fb else PicWidget(**defaults)
        kwargs['form_class'] = SVGAndImageFormField

        try:
            from sb_watermark.models import PicModelForWatermark
            from sb_watermark.widgets import WatermarkWidget
            class_path = '.'.join([self.model.__module__, self.model.__name__])
            field_path = '.'.join([self.model.__module__.split('.', 1)[0], self.model.__name__.lower(), self.attname])
            pic_models = PicModelForWatermark.objects.filter(active=True).values_list('content_type__app_label', 'content_type__model',
                                                                                      'field_name')
            pic_models = ['.'.join([app, model, field]) for app, model, field in pic_models]
            if field_path in pic_models:
                defaults.update({'class_path': class_path, 'field_path': field_path})
                kwargs['widget'] = WatermarkWidget(**defaults)
        except (ProgrammingError, ImportError):
            pass

        return super(PicField, self).formfield(**kwargs)


class PicURLField(models.URLField):
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 255
        self.w = kwargs.pop('w', None)
        self.h = kwargs.pop('h', None)
        super(PicURLField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        kwargs['widget'] = PicURLWidget(w=self.w, h=self.h)
        return super(PicURLField, self).formfield(**kwargs)


def validate_timedelta(value):
    if not isinstance(value, timedelta):
        raise ValidationError('A valid time period is required')


class DurationField(models.DurationField):
    default_validators = [validate_timedelta, ]

    def formfield(self, **kwargs):
        kwargs['widget'] = DurationWidget
        return super(DurationField, self).formfield(**kwargs)
