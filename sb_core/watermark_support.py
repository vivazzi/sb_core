from django.contrib.contenttypes.models import ContentType

try:
    from sb_watermark.models import PicModelForWatermark, Field, Watermark, FieldWatermark
    has_watermark_support = True
except ImportError:
    has_watermark_support = False


def watermark_support(obj, old_obj):
    if has_watermark_support:
        obj_type = ContentType.objects.get_for_model(obj._meta.model)

        for pic_model_for_watermark in PicModelForWatermark.objects.filter(active=True, content_type=obj_type):
            field = Field.objects.get(content_type=obj_type, object_id=obj.id,
                                      field_name=pic_model_for_watermark.field_name)

            old_field = Field.objects.get(content_type=obj_type, object_id=old_obj.id,
                                          field_name=pic_model_for_watermark.field_name)

            for watermark in Watermark.objects.all():
                old_field_watermark = FieldWatermark.objects.get(field=old_field, watermark=watermark)

                field_watermark = FieldWatermark.objects.get(field=field, watermark=watermark)
                field_watermark.active = old_field_watermark.active
                field_watermark.save(update_fields=('active',))
