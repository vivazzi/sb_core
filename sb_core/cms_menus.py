from cms.cms_menus import SoftRootCutter
from menus.menu_pool import menu_pool


def _deregister_menu_pool_modifier(Modifier):
    """
        Taken from django-shop (https://github.com/awesto/django-shop, django-shop/example/myshop/cms_apps.py)
    """
    index = None
    for k, modifier_class in enumerate(menu_pool.modifiers):
        if issubclass(modifier_class, Modifier):
            index = k
    if index is not None:
        # intentionally only modifying the list
        menu_pool.modifiers.pop(index)

_deregister_menu_pool_modifier(SoftRootCutter)
