=======
sb_core
=======

**WARNING: This package is deprecated! Use new package vl_core:** https://github.com/vivazzi/vl_core


--------

--------

--------


sb_core is core of SBL (Service Business Library, https://vits.pro/dev/) and contains some helpful functions (including functionality of SBL plugins).

sb_core and sbl plugins don't support versioning and has unoptimized code sections in some files.

Installation
============

There is no sb_core in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_core


Configuration 
=============

1. Add "sb_core" to INSTALLED_APPS ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        ...
    )


Base plugins
============

Viva IT Studio offers 9 free base plugins that you can use in your projects:

1. Columns. sb_cols - https://bitbucket.org/vivazzi/sb_cols/
2. File. sb_file - https://bitbucket.org/vivazzi/sb_file/
3. Picture. sb_picture - https://bitbucket.org/vivazzi/sb_picture/
4. Gallery. sb_gallery - https://bitbucket.org/vivazzi/sb_gallery/
5. Slider. sb_slider - https://bitbucket.org/vivazzi/sb_slider/
6. Link/Button. sb_link - https://bitbucket.org/vivazzi/sb_link/
7. Forms. sb_form_base - https://bitbucket.org/vivazzi/sb_form_base/
8. Style. sb_style - https://bitbucket.org/vivazzi/sb_style/
9. Snippet. sb_snippet - https://bitbucket.org/vivazzi/sb_snippet/


Documention
============

sb_core contain many functions for different parts of site (admin utilites, handled images, shotcuts and other). Documentation is writing and now here you can read description of some functionality only. You can understand how it works by code in firts time while documentation is not ready.

### admin_utils

Removes "application name" item from breadcrumbs. In most cases application name adds redundancy if django-admin-tools is used in admin interface.

Using:

Add `'sb_core.admin_utils',` to INSTALLED_APPS before `'django.contrib.admin'` ::


    INSTALLED_APPS = (
        ...
        'sb_core.admin_utils',
        'django.contrib.admin',
        ...
    )

### management commands

1. backup - makes backup of database and media files and send it to backup server, if you have them. You should have setting like this ::

    BACKUPS = [{'user': 'user_in_backup_server', 'host': '82.246.10.184'}, ...]

2. create_test_data - creates demo page with sbl base plugins.

3. generate_sitemap - generates sitemap.

4. remove_all_thumbs - removes all thumbnails from field that inherits from PicField.

5. remove_nbsp - remove all `&nbsp;` from body field of Text plugins.


### templatetags

1. sb_default_urls - gets defaults urls to some libraries.

2. sb_math_tags - math operations.

3. sb_menu_tags - contains custom menu and breadcrumbs tags adding helpful parameters.

4. sb_paginator - simplifies the work with the paginator.

5. sb_plugins_tags - contains slider.

6. sb_request - work with GET parameters.

7. sb_tags - very different tags.

8. sb_thumb_tags - working with thumbnails.

### utils

1. admin_utils - contains utilites for admin.

2. check - different checks of site workability (WIP now).

3. generate - genaration of unique char sequence using model field if you need.

4. image - some helpful functions with generation of images with custom text or downloading images by url.

5. os_utils - working with paths and files.

6. progressbar - has ProgressBar class for visual display to console of working functions.

7. sb_utils - very different functions.

8. thumbnail - creates thumbnails with easy-thumbnails or without.

9. url_utils - working with urls.



### cms_models

Contains django-cms plugin mixins.


### folder_mechanism

Utilites for FileModel/PicModel models.


### models

Here you can find useful abstract models for management models with PicField fields.

### template_pool

Functionaly that allows to include template field to model and work with template (for example, for rendering custom cms-plugins).

### thumbnail_processors

Custom thumbnail_processors that uses transparent background and adds parameters for sb_watermark plugin (This plugin is not free).