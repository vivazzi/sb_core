import os
from setuptools import setup, find_packages
from sb_core import __version__

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='sb_core',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    license='MIT License',
    platforms=['OS Independent'],
    description='sb_core is core of SBL (Service Business Library) and contains some helpful functions '
                '(including functionality of SBL plugins)',
    long_description=README,
    url='https://vuspace.pro/',
    download_url='https://bitbucket.org/vivazzi/sb_core/downloads/',
    author='Artem Maltsev',
    author_email='maltsev.artjom@gmail.com',
    keywords='django django-cms sbl core',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', 
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'Django>=1.8.4',
        'django-cms>=3.1.3',
        'bleach>=1.5.0',
        'django-file-resubmit>=0.5.0',
        'easy-thumbnails',
        'requests>=2.7.0',
        'django-select2>=6.0.2',
    ],
)
